package com.mandobiapp.driver.utilities;

import android.support.multidex.BuildConfig;
import android.util.Log;

/**
 * Created by Zeyad Assem on 23/08/16.
 */
public class Logger {
    private static boolean toLog = BuildConfig.DEBUG;
    public static void LOG_I(String tag, String message){
        if(toLog){
            Log.i(tag,message);
        }
    }
}
