package com.mandobiapp.driver;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class ProfitsActivity extends Activity {
    TextViewWithFont date, tor, tpr;
    ImageView cal;
    AsyncHttpClient client = new AsyncHttpClient();
    SimpleDateFormat df;
    Calendar c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profits);
        date = (TextViewWithFont) findViewById(R.id.date);
        tor = (TextViewWithFont) findViewById(R.id.tor);
        tpr = (TextViewWithFont) findViewById(R.id.tpr);
        cal = (ImageView) findViewById(R.id.cal);
        slidemandob.slidemenu(ProfitsActivity.this);
        Gdata.context = "mmc";
         c = Calendar.getInstance();
         df = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat currentDate =new SimpleDateFormat("yyyy-MM-dd",Locale.ENGLISH);
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        date.setText(thisDate);
        client.get(String.format("http://mandobiapp.com/w_m_s/w_profits.php?member_id=%s&t=6&type=recieve&u_t=w&date=%s", Gdata.id, thisDate), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                try {
                    JSONObject j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        tor.setText(j.getString("count"));
                        tpr.setText(j.getString("profits") + " $");
                    } else {
                        tor.setText("0");
                        tpr.setText("0 $");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ProfitsActivity.this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        date.setText(df.format(newDate.getTime()));
                        client.get(String.format("http://mandobiapp.com/w_m_s/w_profits.php?member_id=%s&t=6&type=recieve&u_t=w&date=%s", Gdata.id, df.format(newDate.getTime())), new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(String s) {
                                try {
                                    JSONObject j = new JSONObject(s);
                                    if (j.getString("result").equals("true")) {
                                        tor.setText(j.getString("count"));
                                        tpr.setText(j.getString("profits") + " $");
                                    } else {
                                        tor.setText("0");
                                        tpr.setText("0 $");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
