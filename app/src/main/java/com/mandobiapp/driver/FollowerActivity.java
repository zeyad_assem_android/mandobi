package com.mandobiapp.driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class FollowerActivity extends Activity {
    clientfollwerAdapter orderAdapter;
    ListView list;
    ProgressDialog progressDialog;
    int x = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follower);
        slideclient.slidemenu(FollowerActivity.this);
        Gdata.context = "cf";
        list = (ListView) findViewById(R.id.list);
        orderAdapter = new clientfollwerAdapter(FollowerActivity.this, R.id.list);
        list.setAdapter(orderAdapter);
        new GetOrders().execute();
        list.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // if (scrollState==Gdata.x && list.getCount() <z){
                // new GetOrders().execute();
                // }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (list.getLastVisiblePosition() == totalItemCount - 3) {
                    new GetOrders().execute();
                    // list.invalidateViews();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        Gdata.back(FollowerActivity.this);
    }

    class GetOrders extends AsyncTask<Void, String, JSONArray> {
        String s;
        order news;
        ArrayList<Integer> rr = new ArrayList<Integer>();
        int size = 0;

        @Override
        protected void onPreExecute() {
            if (x == 0) {
                progressDialog = ProgressDialog.show(FollowerActivity.this, getString(R.string.wait), getString(R.string.load), true);
            }
        }

        protected JSONArray doInBackground(Void... voids) {

            JSONArray orders = null;
            JSONParser json = new JSONParser();
            orders = json.makeHttpRequest(
                    String.format("http://mandobiapp.com/c_m_s/view_inbox.php?member_id=%s&type=send&u_t=m&t=2", Gdata.user_id), "GET", "");
            return orders;
        }

        protected void onPostExecute(JSONArray orders) {

            if (orders == null) {
                return;
            }
            if (x == 0) {
                orderAdapter.clear();
            }
            if (x + 20 < orders.length()) {
                size = x + 20;
            } else {
                size = orders.length();
            }

            JSONObject order = null;
            for (int i = x; i < size; i++) {
                try {
                    order = orders.getJSONObject(i);
                    news = new order();
                        news.title = order.getString("msg");
                        news.id = order.getString("mail_id");
                        news.type = order.getString("vehicle_type");
                        news.from = getlocation(order.getString("from_latitude"), order.getString("from_longitude"));
                        news.to = getlocation(order.getString("to_latitude"), order.getString("to_longitude"));
                        news.reciever_id=order.getString("reciever_id");
                        orderAdapter.add(news);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            progressDialog.dismiss();
            x = x + 10;
        }
    }

    public String getlocation(String from, String to) {
        String m = "";
        try {
            Geocoder geocoder;
            List<Address> adresses;
            geocoder = new Geocoder(FollowerActivity.this,
                    Locale.ENGLISH);
            adresses = geocoder.getFromLocation(Double.parseDouble(from),
                    Double.parseDouble(to), 1);
            m = (adresses.get(0).getAddressLine(0) + "  "
                    + adresses.get(0).getAddressLine(1) + "  "
                    + adresses.get(0).getAddressLine(2) + "  " + adresses
                    .get(0).getAddressLine(4));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return m;
    }

}
