package com.mandobiapp.driver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class NeworderActivity extends Activity {
    GoogleMap googleMap;
    MarkerOptions markerOptions;
    LatLng latLng;
    TextViewWithFont send;
    TextView search, delete, search1, delete1, r1, r2, r3;
    AutoCompleteTextView edit, edit1;
    LinearLayout show, lin;
    EditText content,conname,conphone;
    String s = "";
    int t = 0;
    PlacesTask placesTask;
    ParserTask parserTask;
    PlacesTask1 placesTask1;
    ParserTask1 parserTask1;
    Marker marker, from, to;
    String from_lat = "", to_lat = "", from_lon = "", to_lon = "";
    int count = 0;
int x=0;
    ScrollView scroll;
    private static final String LOG_TAG = "ExampleApp";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";

    //------------ make your specific key ------------
//    private static final String API_KEY = "AIzaSyAU9ShujnIg3IDQxtPr7Q1qOvFVdwNmWc4";
    private static final String API_KEY = "AIzaSyDq2Xcu965m5FonCal_MHeNhQxIigutUZY"; //new key
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_neworder);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Gdata.suser_lat = null;
        Gdata.suser_long = null;
        send = (TextViewWithFont) findViewById(R.id.send);
        search = (TextView) findViewById(R.id.search);
        search1 = (TextView) findViewById(R.id.search1);
        delete = (TextView) findViewById(R.id.delete);
        delete1 = (TextView) findViewById(R.id.delete1);
        r1 = (TextView) findViewById(R.id.r1);
        r2 = (TextView) findViewById(R.id.r2);
        r3 = (TextView) findViewById(R.id.r3);
        edit = (AutoCompleteTextView) findViewById(R.id.edit);
        edit1 = (AutoCompleteTextView) findViewById(R.id.edit1);
        edit.setSingleLine();
        edit1.setSingleLine();
        show = (LinearLayout) findViewById(R.id.show);
        lin = (LinearLayout) findViewById(R.id.lin);
        content = (EditText) findViewById(R.id.content);
        conname = (EditText) findViewById(R.id.conname);
        conphone = (EditText) findViewById(R.id.conphone);
        slideclient.slidemenu(NeworderActivity.this);
        Gdata.context = "cn";
        edit.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
        edit1.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.list_item));
//        Intent i = new Intent("com.google.android.c2dm.intent.REGISTER");
//        i.putExtra("app", PendingIntent.getBroadcast(NeworderActivity.this,
//                0, new Intent(), 0));
//        i.putExtra("sender", "829557468935");
//        startService(i);

        //Hashed by Zeyad
//        GCMClientManager pushClientManager = new GCMClientManager(this, "829557468935");
//        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
//            @Override
//            public void onSuccess(String registrationId, boolean isNewRegistration) {
//
//                Log.d("Registration id", registrationId);
//                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
//                        .permitAll().build();
//                StrictMode.setThreadPolicy(policy);
//                String response = null;
//                try {
//                    response = Gdata.h(NeworderActivity.this, "http://mandobiapp.com/c_m_s/insert_access_key.php", registrationId, Gdata.user_id);
//                    if (response.contains("{\"result\":")) {
//                        JSONObject j = new JSONObject(response);
//                        if (j.getString("result").equals("true")) {
//
//                        } else {
//                        }
//                    }else {
//                        Looper.prepare();
//                        Toast t = Toast.makeText(NeworderActivity.this, getString(R.string.sure), Toast.LENGTH_LONG);
//                        t.setGravity(Gravity.CENTER, 0, 0);
//                        t.show();
//                        Looper.loop();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }                }
//
//            @Override
//            public void onFailure(String ex) {
//                super.onFailure(ex);
//            }
//        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (to_lat.equals("") || to_lon.equals("")) {
                    Toast t = Toast.makeText(NeworderActivity.this, getString(R.string.towhere), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                } else if (from_lat.equals("") || from_lon.equals("")) {
                    Toast t = Toast.makeText(NeworderActivity.this, getString(R.string.fromwhere), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                } else if (s.equals("")) {
                    Toast t = Toast.makeText(NeworderActivity.this, getString(R.string.transtype), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                } else if (content.getText().toString().replaceAll(" ", "").equals("")) {
                    Toast t = Toast.makeText(NeworderActivity.this, getString(R.string.ds), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                } else if (conname.getText().toString().replaceAll(" ", "").equals("")) {
                    Toast t = Toast.makeText(NeworderActivity.this, getString(R.string.wcn), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                } else if (conphone.getText().toString().replaceAll(" ", "").equals("")) {
                    Toast t = Toast.makeText(NeworderActivity.this, getString(R.string.wcon), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                } else {
                    Intent i = new Intent(NeworderActivity.this, DistanceActivity.class);
                    Gdata.from_lat = from_lat;
                    Gdata.to_lat = to_lat;
                    Gdata.from_lon = from_lon;
                    Gdata.to_lon = to_lon;
                    Gdata.type = s;
                    Gdata.content = content.getText().toString();
                    Gdata.user_conname = conname.getText().toString();
                    Gdata.user_conphone = conphone.getText().toString();
                    startActivity(i);
                    finish();
                }
            }
        });
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (t == 0) {
                    t = 1;
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,0,1f);
                    lin.setLayoutParams(layoutParams);

                } else {
                    t = 0;
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,0);
                    lin.setLayoutParams(layoutParams);
                }
            }
        });
        r1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s == "") {
                    s = "1";
                    r1.setBackgroundResource(R.drawable.radio_select);
                    r2.setBackgroundResource(R.drawable.radio_unselect);
                    r3.setBackgroundResource(R.drawable.radio_unselect);
                } else {
                    s = "";
                    r1.setBackgroundResource(R.drawable.radio_unselect);
                }
            }
        });
        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s == "") {
                    s = "2";
                    r1.setBackgroundResource(R.drawable.radio_unselect);
                    r2.setBackgroundResource(R.drawable.radio_select);
                    r3.setBackgroundResource(R.drawable.radio_unselect);
                } else {
                    s = "";
                    r2.setBackgroundResource(R.drawable.radio_unselect);
                }
            }
        });
        r3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s == "") {
                    s = "3";
                    r1.setBackgroundResource(R.drawable.radio_unselect);
                    r2.setBackgroundResource(R.drawable.radio_unselect);
                    r3.setBackgroundResource(R.drawable.radio_select);
                } else {
                    s = "";
                    r3.setBackgroundResource(R.drawable.radio_unselect);
                }
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit.setText("");
            }
        });
        delete1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit1.setText("");
            }
        });
        edit.setThreshold(1);
//        edit.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                placesTask = new PlacesTask();
//                placesTask.execute(s.toString());
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count,
//                                          int after) {
//                // TODO Auto-generated method stub
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                // TODO Auto-generated method stub
//            }
//        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String location = edit.getText().toString();
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (location != null && !location.equals("")) {
                    new GeocoderTask().execute(location);
                }
            }
        });
        edit1.setThreshold(1);
        edit1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask1 = new PlacesTask1();
                placesTask1.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        search1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String location = edit1.getText().toString();
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                if (location != null && !location.equals("")) {
                    new GeocoderTask1().execute(location);
                }
            }
        });
        edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    String location = edit.getText().toString();
                    getWindow().setSoftInputMode(
                            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    if (location != null && !location.equals("")) {
                        new GeocoderTask().execute(location);
                    }
                    return true;
                }else  if (i == EditorInfo.IME_ACTION_NEXT) {
                    String location = edit.getText().toString();
                    getWindow().setSoftInputMode(
                            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    if (location != null && !location.equals("")) {
                        new GeocoderTask().execute(location);
                    }
                    return true;
                }
                return false;
            }
        });
        edit1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    String location = edit1.getText().toString();
                    getWindow().setSoftInputMode(
                            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    if (location != null && !location.equals("")) {
                        new GeocoderTask1().execute(location);
                    }
                    return true;
                }else  if (i == EditorInfo.IME_ACTION_NEXT) {
                    String location = edit.getText().toString();
                    getWindow().setSoftInputMode(
                            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    if (location != null && !location.equals("")) {
                        new GeocoderTask().execute(location);
                    }
                    return true;
                }
                return false;            }
        });
        try {
            if (googleMap == null) {
                //                Hashed by zeyad
//                googleMap = ((MapFragment) getFragmentManager().findFragmentById(
//                        R.id.map)).getMap();
            }
            googleMap.setMyLocationEnabled(true);
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {

                    LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                    Gdata.suser_lat = Double.toString(loc.latitude);
                    Gdata.suser_long = Double.toString(loc.longitude);
                    if (count == 0) {
                        from_lat = Double.toString(loc.latitude);
                        from_lon = Double.toString(loc.longitude);
                        marker = googleMap.addMarker(new MarkerOptions().position(loc).title(
                                "MY LOCATION").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).draggable(true));
                        // marker.showInfoWindow();
                        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                        googleMap.getUiSettings().setZoomGesturesEnabled(true);
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(loc).zoom(14).build();
                        googleMap.animateCamera(CameraUpdateFactory
                                .newCameraPosition(cameraPosition));
                        edit.setText(getlocation(Double.toString(loc.latitude), Double.toString(loc.longitude)));
                        count++;
                    }

                }
            };
            googleMap.setOnMyLocationChangeListener(myLocationChangeListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
        googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                if (marker.getTitle().equals("MY LOCATION")) {
                    Gdata.suser_lat = Double.toString(marker.getPosition().latitude);
                    Gdata.suser_long = Double.toString(marker.getPosition().longitude);
                } else if (marker.getTitle().equals("FROM LOCATION")) {
                    from_lat = Double.toString(marker.getPosition().latitude);
                    from_lon = Double.toString(marker.getPosition().longitude);
                    edit.setText(getlocation(Double.toString(marker.getPosition().latitude), Double.toString(marker.getPosition().longitude)));
                } else if (marker.getTitle().equals("TO LOCATION")) {
                    to_lat = Double.toString(marker.getPosition().latitude);
                    to_lon = Double.toString(marker.getPosition().longitude);
                    edit1.setText(getlocation(Double.toString(marker.getPosition().latitude), Double.toString(marker.getPosition().longitude)));
                }
            }
        });

    }

    private class GeocoderTask extends AsyncTask<String, Void, List<Address>> {

        @Override
        protected List<Address> doInBackground(String... locationName) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;

            try {
                // Getting a maximum of 3 Address that matches the input text
                addresses = geocoder.getFromLocationName(locationName[0], 3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return addresses;
        }


        @Override
        protected void onPostExecute(List<Address> addresses) {

            if (addresses == null || addresses.size() == 0) {
                Toast.makeText(getBaseContext(), "No Location found", Toast.LENGTH_SHORT).show();
            }

            // Adding Markers on Google Map for each matching address
            for (int i = 0; i < addresses.size(); i++) {

                Address address = (Address) addresses.get(i);
                if (from_lon != "" && x!=0) {
                    from.remove();
                }
                // Creating an instance of GeoPoint, to display in Google Map
                latLng = new LatLng(address.getLatitude(), address.getLongitude());
                from_lat = Double.toString(address.getLatitude());
                from_lon = Double.toString(address.getLongitude());
                String addressText = String.format("%s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getCountryName());

                // Locate the first location
                if (i == 0) {
                    from = googleMap.addMarker(new MarkerOptions().position(latLng).title(
                            "FROM LOCATION").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).draggable(true));
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLng).zoom(14).build();
                    googleMap.moveCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));

                }
                x++;
            }
        }
    }

    private String downloadUrl(String strUrl) throws IOException{
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
           // Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
//            String key = "key=AIzaSyAU9ShujnIg3IDQxtPr7Q1qOvFVdwNmWc4";
            String key = API_KEY;

            String input="";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }


            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input+"&"+types+"&"+sensor+"&"+key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;

            try{
                // Fetching the data from web service in background
                data = downloadUrl(url);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }


    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[] { "description"};
            int[] to = new int[] { android.R.id.text1 };

            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

            // Setting the adapter
            edit.setAdapter(adapter);
        }
    }


    private class GeocoderTask1 extends AsyncTask<String, Void, List<Address>> {

        @Override
        protected List<Address> doInBackground(String... locationName) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getBaseContext());
            List<Address> addresses = null;

            try {
                // Getting a maximum of 3 Address that matches the input text
                addresses = geocoder.getFromLocationName(locationName[0], 3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return addresses;
        }


        @Override
        protected void onPostExecute(List<Address> addresses) {

            if (addresses == null || addresses.size() == 0) {
                Toast.makeText(getBaseContext(), "No Location found", Toast.LENGTH_SHORT).show();
            }

            if (to_lon != "") {
                to.remove();
            }
            // Adding Markers on Google Map for each matching address
            for (int i = 0; i < addresses.size(); i++) {

                Address address = (Address) addresses.get(i);

                // Creating an instance of GeoPoint, to display in Google Map
                latLng = new LatLng(address.getLatitude(), address.getLongitude());
                to_lat = Double.toString(address.getLatitude());
                to_lon = Double.toString(address.getLongitude());
                String addressText = String.format("%s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getCountryName());
                if (i == 0) {

                    to = googleMap.addMarker(new MarkerOptions().position(latLng).title(
                            "TO LOCATION").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)).draggable(true));
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLng).zoom(14).build();
                    googleMap.moveCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));

                }
            }
        }
    }

    private String downloadUrl1(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            //Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask1 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
//            String key = "key=AIzaSyAU9ShujnIg3IDQxtPr7Q1qOvFVdwNmWc4";
            String key = API_KEY;

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }


            // place type to be searched
            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&" + sensor + "&key=" + key;

            // Output format
            String output = "json";

            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;

            try {
                // Fetching the data from web service in background
                data = downloadUrl1(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask1 = new ParserTask1();

            // Starting Parsing the JSON string returned by Web Service
            parserTask1.execute(result);
        }
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask1 extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};

            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

            // Setting the adapter
            edit1.setAdapter(adapter);
        }
    }

    public String getlocation(String from, String to) {
        String m = "";
        try {
            Geocoder geocoder;
            List<Address> adresses;
            geocoder = new Geocoder(NeworderActivity.this,
                    Locale.ENGLISH);
            adresses = geocoder.getFromLocation(Double.parseDouble(from),
                    Double.parseDouble(to), 1);
            m = (adresses.get(0).getAddressLine(0) + "  "
                    + adresses.get(0).getAddressLine(1) + "  "
                    + adresses.get(0).getAddressLine(2) + "  " + adresses
                    .get(0).getAddressLine(4));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return m;
    }

    @Override
    public void onBackPressed() {
        Gdata.back(NeworderActivity.this);
    }
    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }

}
