package com.mandobiapp.driver;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class MyCdmReceiver extends BroadcastReceiver {
    NotificationManager nm;
    Intent notificationIntent;
    private static String KEY = "AIzaSyDLBv1HHMjncfhgTIK67p7YcTUpsPrTFro";
    private static String REGISTRATION_KEY = "registrationKey";

    @SuppressWarnings("unused")
    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        if (intent.getAction().equals(
                "com.google.android.c2dm.intent.REGISTRATION")) {
            handleRegistration(context, intent);
        } else if (intent.getAction().equals(
                "com.google.android.c2dm.intent.RECEIVE")) {
            handleMessage(context, intent);
        }
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("NewApi")
    private void handleRegistration(Context context, Intent intent) {
        String registration = intent.getStringExtra("registration_id");
        if (intent.getStringExtra("error") != null) {
            // Registration failed, should try again later.
            Log.d("c2dm", "registration failed");
            String error = intent.getStringExtra("error");
            if (error == "SERVICE_NOT_AVAILABLE") {
                Log.d("c2dm", "SERVICE_NOT_AVAILABLE");
            } else if (error == "ACCOUNT_MISSING") {
                Log.d("c2dm", "ACCOUNT_MISSING");
            } else if (error == "AUTHENTICATION_FAILED") {
                Log.d("c2dm", "AUTHENTICATION_FAILED");
            } else if (error == "TOO_MANY_REGISTRATIONS") {
                Log.d("c2dm", "TOO_MANY_REGISTRATIONS");
            } else if (error == "INVALID_SENDER") {
                Log.d("c2dm", "INVALID_SENDER");
            } else if (error == "PHONE_REGISTRATION_ERROR") {
                Log.d("c2dm", "PHONE_REGISTRATION_ERROR");
            }
        } else if (intent.getStringExtra("unregistered") != null) {
            // unregistration done, new messages from the authorized sender will
            // be rejected
            Log.d("c2dm", "unregistered");

        } else if (registration != null) {
            Log.d("c2dm", registration);
            Editor editor = context.getSharedPreferences(KEY,
                    Context.MODE_PRIVATE).edit();
            editor.putString(REGISTRATION_KEY, registration);
            editor.commit();

            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
                String response = null;
                try {
                    response = Gdata.h(context,"http://mandobiapp.com/c_m_s/insert_access_key.php", registration, Gdata.id);
                    JSONObject j = new JSONObject(response);
                    if (j.getString("result").equals("true")) {

                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(context,context.getString(R.string.check),Toast.LENGTH_LONG).show();
            }
            // Send the registration ID to the 3rd party site that is sending
            // the messages.
            // This should be done in a separate thread.
            // When done, remember that all registration is done.
        }
    }

    @SuppressWarnings({"static-access", "deprecation"})
    private void handleMessage(Context context, Intent intent) {
//        if (intent.getExtras().getString("type").equals("0")) {
//            notificationIntent = new Intent(context, ClientorderActivity.class);
//            String key = intent.getExtras().getString("message");
//            Gdata.user_id = intent.getExtras().getString("member_id");
//            Gdata.message_id = intent.getExtras().getString("msg_id");
//            String title = intent.getExtras().getString("title");
//            nm = (NotificationManager) context
//                    .getSystemService(context.NOTIFICATION_SERVICE);
//            Notification no = new Notification(R.drawable.ic_launcher, key,
//                    System.currentTimeMillis());
//            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                    | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
//            PendingIntent pend = PendingIntent.getActivity(context, 0,
//                    notificationIntent,
//                    PendingIntent.FLAG_ONE_SHOT);
//            no.setLatestEventInfo(context, title, key, pend);
//            no.flags |= Notification.FLAG_AUTO_CANCEL;
//            no.defaults |= Notification.DEFAULT_SOUND;
//            no.defaults |= Notification.DEFAULT_VIBRATE;
//            nm.notify(Integer.parseInt(Gdata.message_id), no);
//        }
//        else if (intent.getExtras().getString("type").equals("1")) {
//            notificationIntent = new Intent(context, arraive.class);
//                    String key = intent.getExtras().getString("message")+ "\n"+ context.getString(R.string.discount)+ intent.getExtras().getString("fees_value")+" "+context.getString(R.string.omla);
//            Gdata.user_id = intent.getExtras().getString("member_id");
//            Gdata.message_id = intent.getExtras().getString("msg_id");
//            String title = intent.getExtras().getString("title");
//            Gdata.title=key;
//            nm = (NotificationManager) context
//                    .getSystemService(context.NOTIFICATION_SERVICE);
//            Notification no = new Notification(R.drawable.ic_launcher, key,
//                    System.currentTimeMillis());
//            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                    | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
//            PendingIntent pend = PendingIntent.getActivity(context, 0,
//                    notificationIntent,
//                    PendingIntent.FLAG_ONE_SHOT);
//            no.setLatestEventInfo(context, title, key, pend);
//            no.flags |= Notification.FLAG_AUTO_CANCEL;
//            no.defaults |= Notification.DEFAULT_SOUND;
//            no.defaults |= Notification.DEFAULT_VIBRATE;
//            nm.notify(Integer.parseInt(Gdata.message_id), no);
//        } else if (intent.getExtras().getString("type").equals("2")) {
//            String key = context.getString(R.string.cancel)+intent.getExtras().getString("message");
//            Gdata.user_id = intent.getExtras().getString("member_id");
//            Gdata.message_id = intent.getExtras().getString("msg_id");
//            String title = intent.getExtras().getString("title");
//            nm = (NotificationManager) context
//                    .getSystemService(context.NOTIFICATION_SERVICE);
//            Notification no = new Notification(R.drawable.ic_launcher, key,
//                    System.currentTimeMillis());
//            PendingIntent pend = PendingIntent.getActivity(context, 0,
//                    new Intent(),
//                    PendingIntent.FLAG_ONE_SHOT);
//            no.setLatestEventInfo(context, title, key, pend);
//            no.flags |= Notification.FLAG_AUTO_CANCEL;
//            no.defaults |= Notification.DEFAULT_SOUND;
//            no.defaults |= Notification.DEFAULT_VIBRATE;
//            nm.notify(Integer.parseInt(Gdata.message_id), no);
//        }else if (intent.getExtras().getString("type").equals("3")) {
//            notificationIntent = new Intent(context, publicnotificition.class);
//            String key = intent.getExtras().getString("message");
//            Gdata.message_id = intent.getExtras().getString("msg_id");
//            String title = intent.getExtras().getString("title");
//            Gdata.t = intent.getExtras().getString("title");
//            Gdata.body= intent.getExtras().getString("message");
//            nm = (NotificationManager) context
//                    .getSystemService(context.NOTIFICATION_SERVICE);
//            Notification no = new Notification(R.drawable.ic_launcher, key,
//                    System.currentTimeMillis());
//            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                    | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
//            PendingIntent pend = PendingIntent.getActivity(context, 0,
//                    notificationIntent,
//                    PendingIntent.FLAG_ONE_SHOT);
//            no.setLatestEventInfo(context, title, key, pend);
//            no.flags |= Notification.FLAG_AUTO_CANCEL;
//            no.defaults |= Notification.DEFAULT_SOUND;
//            no.defaults |= Notification.DEFAULT_VIBRATE;
//            nm.notify(Integer.parseInt(Gdata.message_id), no);
//        }


    }
}