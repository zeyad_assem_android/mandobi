package com.mandobiapp.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DistanceActivity extends Activity {
    TextView typeimage, cancel, accept;
    TextViewWithFont type, des, value, per,conname,conphone;
    Double s;
    GoogleMap mmap;
    Marker marker;
    AsyncHttpClient client = new AsyncHttpClient();
    int x;
    float distance;
    String dist;
    ProgressDialog progressDialog;
    Double y ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);
        typeimage = (TextView) findViewById(R.id.typeimage);
        cancel = (TextView) findViewById(R.id.cancel);
        accept = (TextView) findViewById(R.id.accept);
        type = (TextViewWithFont) findViewById(R.id.type);
        des = (TextViewWithFont) findViewById(R.id.distance);
        value = (TextViewWithFont) findViewById(R.id.value);
        per = (TextViewWithFont) findViewById(R.id.per);
        conname = (TextViewWithFont) findViewById(R.id.conname);
        conphone = (TextViewWithFont) findViewById(R.id.conphone);
        conname.setText(Gdata.user_conname);
        conphone.setText(Gdata.user_conphone);
        if (Gdata.type.equals("1")) {
            typeimage.setBackgroundResource(R.drawable.smal_car);
            type.setText(getString(R.string.small));
        } else if (Gdata.type.equals("2")) {
            typeimage.setBackgroundResource(R.drawable.med_car);
            type.setText(getString(R.string.medium));
        } else {
            typeimage.setBackgroundResource(R.drawable.large_car);
            type.setText(getString(R.string.large));
        }
        slideclient.slidemenu(DistanceActivity.this);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = ProgressDialog.show(DistanceActivity.this, getString(R.string.wait), getString(R.string.load), true);
                client.get(String.format("http://mandobiapp.com/c_m_s/search_in_location.php?longitude=%s&latitude=%s&member_id=%s&from_longitude=%s&from_latitude=%s&to_longitude=%s&to_latitude=%s&vehicle_type=%s&radius=40&lang=1&msg=%s&price_move=%s&recipient_tel=%s&recipient_user=%s", Gdata.suser_long, Gdata.suser_lat, Gdata.user_id, Gdata.from_lon, Gdata.from_lat, Gdata.to_lon, Gdata.to_lat, Gdata.type, Gdata.content.replaceAll(" ", "%20"),Integer.toString(y.intValue()),Gdata.user_conphone.replaceAll(" ", "%20"),Gdata.user_conname.replaceAll(" ", "%20")), new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String s) {
                        try {
                            JSONObject jo = new JSONObject(s);
                            if (jo.getString("result").equals("true")) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(DistanceActivity.this);
                                builder.setTitle(getString(R.string.success));
                                builder.setMessage(getString(R.string.orderhas));
                                builder.show();
                                new Thread(){
                                    @Override
                                    public void run() {
                                        try {
                                            sleep(2500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }finally {
                                            Intent i = new Intent(DistanceActivity.this,NeworderActivity.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    }
                                }.start();

                            } else {
                                Toast t = Toast.makeText(DistanceActivity.this, getString(R.string.nomandob), Toast.LENGTH_LONG);
                                t.setGravity(Gravity.CENTER, 0, 0);
                                t.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                });
            }
        });
//        s = Gdata.distance(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon), Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon));
//        distance.setText(Double.toString(s) + " " + getString(R.string.km));
//        Double m = s * 5;
//        value.setText(Integer.toString(m.intValue()) + "$");
        Location l1 = new Location("One");
        l1.setLatitude(Double.parseDouble(Gdata.from_lat));
        l1.setLongitude(Double.parseDouble(Gdata.from_lon));

        Location l2 = new Location("Two");
        l2.setLatitude(Double.parseDouble(Gdata.to_lat));
        l2.setLongitude(Double.parseDouble(Gdata.to_lon));

        distance = l1.distanceTo(l2);
        dist = distance + " M";
        client.get("http://mandobiapp.com/c_m_s/detail.php?d=9", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                try {
                    JSONObject j = new JSONObject(s);
                    JSONArray jary = j.getJSONArray("data");
                    JSONObject jo = jary.getJSONObject(0);
                    x = jo.getInt("detail_text");
                    per.setText(Integer.toString(x) + " $");
                    if (distance > 1000.0f) {
                        distance = distance / 1000.0f;
                        dist = distance + " KM";
                         y = Double.parseDouble(Float.toString(distance)) * x;
                        value.setText(Integer.toString(y.intValue()) + "$");

                    } else {
                        distance = distance / 1000.0f;
                         y = Double.parseDouble(Float.toString(distance)) * x;
                        value.setText(Integer.toString(y.intValue()) + "$");
                    }
                    Gdata.context = "cd";
                    des.setText(dist);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            if (mmap == null) {
//                Hashed by zeyad
//                mmap = ((MapFragment) getFragmentManager().findFragmentById(
//                        R.id.map)).getMap();
            }
            mmap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mmap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon))).title(
                    "FROM LOCATION").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
            marker = mmap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon))).title(
                    "TO LOCATION").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
            mmap.getUiSettings().setMyLocationButtonEnabled(true);
            mmap.getUiSettings().setZoomGesturesEnabled(true);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon))).zoom(12).build();
            mmap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
//        mmap.addPolyline(new PolylineOptions()
//                .add(new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon)), new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon)))
//                .width(10)
//                .color(Color.parseColor("#f9b21a")).geodesic(true));
        } catch (Exception e) {
            e.printStackTrace();
        }
        LatLng origin = new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon));
        LatLng dest = new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon));
        String url = getDirectionsUrl(origin, dest);
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DistanceActivity.this,ClientprofileActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + ","
                + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + parameters;
        return url;
    }

    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                parser parser = new parser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            Log.e("results", result + "");
            if (result.size() < 1) {
                Toast.makeText(DistanceActivity.this, "No Points", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);
                Log.e("points", path + "");
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    if (j == 0) {

                    } else if (j == 1) {
                    } else if (j > 1) {
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                }
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.parseColor("#f9b21a"));
            }
            mmap.addPolyline(lineOptions);
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(DistanceActivity.this, NeworderActivity.class);
        startActivity(i);
        finish();
    }
}
