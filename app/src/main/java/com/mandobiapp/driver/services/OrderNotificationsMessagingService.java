package com.mandobiapp.driver.services;

import android.content.Intent;

import com.mandobiapp.driver.ui.activities.driver.orders.OrderDetails;
import com.mandobiapp.driver.utilities.Logger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by Zeyad Assem on 21/09/16.
 */

public class OrderNotificationsMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
//            Logger.Lo(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Logger.LOG_I("notification", "Message Notification Body: " + remoteMessage.getNotification().getBody());

//            sendNotification( remoteMessage);

            Intent broadcast = new Intent();
            Map<String, String> map  = remoteMessage.getData();
            String data = map.get("client_id") + " - "+map.get("msg_id")+" - "+map.get("type");
            broadcast.putExtra("client_id", map.get("client_id"));
            broadcast.putExtra("msg_id", map.get("msg_id"));
            broadcast.putExtra("type",map.get("type"));
            broadcast.setAction("OPEN_NEW_ACTIVITY");
            sendBroadcast(broadcast);
        }
    }

    private void sendNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, OrderDetails.class);

        Map<String, String> map  = remoteMessage.getData();
        intent.putExtra("client_id", map.get("client_id"));
        intent.putExtra("msg_id", map.get("msg_id"));
        intent.putExtra("type",map.get("type"));


//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);

    }
}
