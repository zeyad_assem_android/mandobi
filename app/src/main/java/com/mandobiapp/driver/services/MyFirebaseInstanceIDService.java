package com.mandobiapp.driver.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import com.mandobiapp.driver.utilities.Logger;

/**
 * Created by Zeyad Assem on 19/09/16.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Logger.LOG_I("token message", FirebaseInstanceId.getInstance().getToken());
    }
}
