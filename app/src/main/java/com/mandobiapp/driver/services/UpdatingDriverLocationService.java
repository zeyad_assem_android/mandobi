package com.mandobiapp.driver.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.widget.Toast;

/**
 * Created by Zeyad Assem on 28/09/16.
 */
public class UpdatingDriverLocationService extends IntentService {
    Handler handler ;

    public UpdatingDriverLocationService() {
        super("UpdatingDriverLocationService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String driverId = intent.getStringExtra("driver_id");
            updateDriverLocation();
        }
    }

    private void updateDriverLocation() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        },300000);
    }

//    private Location getCurrentLocation();


}
