package com.mandobiapp.driver.ui.activities.driver.language;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.managers.SharedPreferenceManager;
import com.mandobiapp.driver.ui.activities.base_activities.BaseAppCompatActivity;
import com.mandobiapp.driver.ui.activities.driver.main.DriverMainActivity;
import com.mandobiapp.driver.ui.activities.uiUitilities.RobotoTextView;

public class ChangeLanguageActivity extends BaseAppCompatActivity {
    RobotoTextView englishLanguage, arabicLanguage;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_language);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        initActionBar();
        context= this;
        englishLanguage = (RobotoTextView)findViewById(R.id.english_language);
        englishLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceManager.changeLanguage("en");
                Toast.makeText(context, "Language changed", Toast.LENGTH_SHORT).show();
                openMainActivity();
                finish();
            }
        });

        arabicLanguage = (RobotoTextView)findViewById(R.id.arabic_language);
        arabicLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferenceManager.changeLanguage("ar");
                Toast.makeText(context, "تغيرت اللغة", Toast.LENGTH_SHORT).show();
                openMainActivity();
                finish();
            }
        });

    }

    private void openMainActivity() {
        Intent intent = new Intent(this, DriverMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    private void initActionBar() {
        final ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
