package com.mandobiapp.driver.ui.activities.uiUitilities;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Zeyad Assem on 02/09/16.
 */
public class RobotoTextView extends TextView{
    private Typeface typeface;
    public RobotoTextView(Context context) {
        this(context,null);
    }

    public RobotoTextView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public RobotoTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            typeface = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Regular.ttf");
            setTypeface(typeface);
        }
    }
}
