package com.mandobiapp.driver.ui.activities.driver.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mandobiapp.driver.Gdata;
import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.ui.activities.base_activities.BaseAppCompatActivity;
import com.mandobiapp.driver.ui.activities.driver.main.DriverMainActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class DriverEditPassword extends BaseAppCompatActivity implements View.OnClickListener {
    EditText password, rePassword;
    Button submit;
    Context context;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_edit_password);
        context = this;
        initViews();
    }

    private void initViews() {
        password = (EditText)findViewById(R.id.password);
        rePassword = (EditText)findViewById(R.id.re_password);
        submit = (Button)findViewById(R.id.submit);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit:
                progressDialog = ProgressDialog.show(context, getString(R.string.wait), getString(R.string.load), true);
                if((!password.getText().toString().isEmpty()) && (!rePassword.getText().toString().isEmpty())){
                    if(password.getText().toString().equals(rePassword.getText().toString())){
                        if(password.getText().toString().length() >=7){
                            AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                            RequestParams requestParams = new RequestParams();
                            requestParams.put("driver_id", Gdata.id);
                            requestParams.put("password",password.getText().toString());
                            asyncHttpClient.post(URLConstants.BASE_URL+"w_m_s/update_password.php",requestParams, new AsyncHttpResponseHandler(){
                                @Override
                                public void onSuccess(int i, String s) {
                                    try {
                                        JSONObject response = new JSONObject(s);
                                        if(response.getString("result").equals("true")){
                                            Toast.makeText(context,R.string.success, Toast.LENGTH_SHORT).show();
                                            progressDialog.dismiss();
                                            Intent intent = new Intent(context, DriverMainActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            startActivity(intent);
                                        }else{
                                            progressDialog.dismiss();
                                            Toast.makeText(context,response.getString("data"), Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Throwable throwable, String s) {
                                    super.onFailure(throwable, s);
                                    progressDialog.dismiss();
                                    Toast.makeText(context,R.string.something_wrong_message, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }else {
                            progressDialog.dismiss();
                            Toast.makeText(context,R.string.short_password, Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        progressDialog.dismiss();
                        Toast.makeText(context,R.string.pnot, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    progressDialog.dismiss();
                    Toast.makeText(context,R.string.empty_password, Toast.LENGTH_SHORT).show();
                }
        }
    }
}
