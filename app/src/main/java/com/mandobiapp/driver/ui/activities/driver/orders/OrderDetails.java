package com.mandobiapp.driver.ui.activities.driver.orders;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mandobiapp.driver.Gdata;
import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.order;
import com.mandobiapp.driver.ui.activities.driver.main.DriverMainActivity;
import com.mandobiapp.driver.ui.activities.uiUitilities.RobotoTextView;

public class OrderDetails extends AppCompatActivity implements View.OnClickListener{
    RobotoTextView activityTitle, emptyText;
    ProgressBar progressBar;
    LinearLayout cardView;
    Bundle intentData;
    LinearLayout acceptRejectLayout, navigateFinishLayout;
    Button acceptButton, rejectButton, navigateButton, finishButton;
    AsyncHttpClient client;
    Context context;
    order globalOrder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        this.context = this;
        intentData = new Bundle();
        getIntentData();
        initViews();
        initOrderDetailsView();

    }

    private void initOrderDetailsView() {
        client = new AsyncHttpClient();
        client.get(URLConstants.BASE_URL+"c_m_s/view_msg_by_id.php?msg_id="+intentData.getString("msg_id")+"&u_t=w&type=receive",new AsyncHttpResponseHandler(){
            @Override
            public void onSuccess(String s) {
                JSONObject response = null;
                JSONArray orders = null;
                JSONObject order = null;
                String orderType= null;
                try {
                    response = new JSONObject(s);
                    orders = response.getJSONArray("data");
                    if(orders.length() >0){
                        order = orders.getJSONObject(0);
                        order orderDataModel = new order();
                        orderDataModel.id = order.getString("mail_id");
                        orderDataModel.name = order.getString("f_name");
                        orderDataModel.date = order.getString("date_insert");
                        orderDataModel.from = order.getString("from");
                        orderDataModel.from_lat = order.getString("from_latitude");
                        orderDataModel.from_lon = order.getString("from_longitude");
                        orderDataModel.to = order.getString("to");
                        orderDataModel.to_lat = order.getString("to_latitude");
                        orderDataModel.to_lon = order.getString("to_longitude");
                        orderDataModel.sender_id = order.getString("sender_id");
                        orderDataModel.reciever_id = order.getString("reciever_id");
                        orderType = order.getString("type");
                        OrderItemViewModel orderItemViewModel = new OrderItemViewModel(context);
                        orderItemViewModel.bindData(orderDataModel);
                        globalOrder = orderDataModel;
                        cardView.addView(orderItemViewModel);
                        cardView.setVisibility(View.VISIBLE);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
                initButtons(orderType);
            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                progressBar.setVisibility(View.GONE);
                emptyText.setVisibility(View.VISIBLE);
            }
        });

    }

    private void initButtons(String orderType) {
        if(orderType!=null){
            if(orderType.equals("0")){
                acceptRejectLayout.setVisibility(View.VISIBLE);
            }else if(orderType.equals("2")){
                navigateFinishLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    private void initViews() {
        activityTitle = (RobotoTextView)findViewById(R.id.activity_title);
        if(intentData.containsKey("type")){
            if(intentData.get("type").equals("details")){
                activityTitle.setText(this.getResources().getText(R.string.order_details));
            }
        }
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        emptyText = (RobotoTextView)findViewById(R.id.empty_text);
        cardView= (LinearLayout) findViewById(R.id.basic_info_container);
        acceptRejectLayout = (LinearLayout)findViewById(R.id.accept_reject_layout);
        navigateFinishLayout = (LinearLayout)findViewById(R.id.navigate_finish_layout);
        acceptButton = (Button)findViewById(R.id.accept_button);
        rejectButton = (Button)findViewById(R.id.reject_button);
        navigateButton = (Button)findViewById(R.id.navigate_button);
        finishButton = (Button)findViewById(R.id.finish_button);
        acceptButton.setOnClickListener(this);
        rejectButton.setOnClickListener(this);
        navigateButton.setOnClickListener(this);
        finishButton.setOnClickListener(this);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        initActionBar();
    }

    private void initActionBar() {
        final ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void getIntentData() {
        intentData = getIntent().getExtras();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.accept_button:
                Intent intent = new Intent(this,OrderFareActivity.class);
                intent.putExtra("sender_id",globalOrder.sender_id);
                intent.putExtra("reciever_id",globalOrder.reciever_id);
                intent.putExtra("msg_id", globalOrder.id);
                context.startActivity(intent);
                break;

            case R.id.reject_button:
                AsyncHttpClient client = new AsyncHttpClient();
                client.get(URLConstants.BASE_URL+"c_m_s/re_reply_search_cancel.php?sender_id="+ globalOrder.sender_id+"&reciever_id="+globalOrder.reciever_id+"&refuse= 0&msg_id="+globalOrder.id,new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(String s) {
                    }

                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        Toast.makeText(context,R.string.something_wrong_message,Toast.LENGTH_SHORT).show();
                    }
                });
                break;

            case R.id.navigate_button:
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+globalOrder.to_lat+","+globalOrder.to_lon);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
                break;

            case R.id.finish_button:
                AsyncHttpClient client2 = new AsyncHttpClient();
                RequestParams requestParams = new RequestParams();
                requestParams.put("msg_id",globalOrder.id);
                requestParams.put("driver_id", globalOrder.reciever_id);
                requestParams.put("client_id",globalOrder.sender_id);
                client2.post(URLConstants.BASE_URL+"w_m_s/finish_order.php",requestParams,new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(int i, String s) {
                        Toast.makeText(context,R.string.order_finish,Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, DriverMainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        context.startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        Toast.makeText(context,R.string.something_wrong_message,Toast.LENGTH_LONG).show();
                    }
                });
                break;



                
        }
    }
}
