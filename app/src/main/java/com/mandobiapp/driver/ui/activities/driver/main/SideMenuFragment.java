package com.mandobiapp.driver.ui.activities.driver.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.ui.activities.driver.language.ChangeLanguageActivity;
import com.squareup.picasso.Picasso;

import com.mandobiapp.driver.Gdata;
import com.mandobiapp.driver.LoginActivity;

import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.managers.SharedPreferenceManager;
import com.mandobiapp.driver.ui.activities.driver.orders.OrdersMainActivity;
import com.mandobiapp.driver.ui.activities.driver.profile.DriverProfileActivity;
import com.mandobiapp.driver.ui.activities.driver.profit.DriverProfitActivity;
import com.mandobiapp.driver.ui.activities.uiUitilities.RobotoTextView;

import java.util.Locale;

/**
 * Created by Zeyad Assem on 02/09/16.
 */
public class SideMenuFragment extends Fragment implements View.OnClickListener{
    Context context;
    LinearLayout myProfile, myOrders, myProfit, changeLanguage;
    Button logout;
    ImageView userIamge;
    RobotoTextView userName;

    public SideMenuFragment() {

    }


    public static SideMenuFragment newInstance() {
        return new SideMenuFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_side_menu, container, false);
        userIamge = (ImageView)mainView.findViewById(R.id.user_image);
        userName = (RobotoTextView)mainView.findViewById(R.id.user_name);
        initViews(mainView);
        return mainView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getContext();
        userName.setText(Gdata.name);
        Picasso.with(getContext()).load(URLConstants.IMAGES_BASE_URL+Gdata.photo).placeholder(R.drawable.avatar_placeholder).into(userIamge);
    }

    private void initViews(View mainView){
        myProfile = (LinearLayout)mainView.findViewById(R.id.my_profile);
        myOrders = (LinearLayout)mainView.findViewById(R.id.my_orders);
        myProfit = (LinearLayout)mainView.findViewById(R.id.my_profit);
        changeLanguage = (LinearLayout)mainView.findViewById(R.id.change_language);
        logout = (Button)mainView.findViewById(R.id.logout);

        myProfile.setOnClickListener(this);
        myOrders.setOnClickListener(this);
        myProfit.setOnClickListener(this);
        changeLanguage.setOnClickListener(this);
        logout.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.my_profile:
                intent = new Intent(context, DriverProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.my_orders:
                intent = new Intent(context, OrdersMainActivity.class);
                startActivity(intent);
                break;
            case R.id.my_profit:
                intent = new Intent(context, DriverProfitActivity.class);
                startActivity(intent);
                break;
            case R.id.change_language:
                intent = new Intent(context,ChangeLanguageActivity.class);
                startActivity(intent);
                break;
            case R.id.logout:
                intent = new Intent(context, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Gdata.backlist.removeAll(Gdata.backlist);
                Gdata.count=0;
                SharedPreferenceManager.getInstance(getContext()).clearAllSharedPreferenceData();
                AsyncHttpClient asyncHttpClient= new AsyncHttpClient();
                RequestParams requestParams = new RequestParams();
                requestParams.put("member_id",Gdata.id);
                asyncHttpClient.post(URLConstants.BASE_URL+"w_m_s/logout_update.php",requestParams,new AsyncHttpResponseHandler());
                startActivity(intent);
                getActivity().finish();
                break;

        }
    }
}
