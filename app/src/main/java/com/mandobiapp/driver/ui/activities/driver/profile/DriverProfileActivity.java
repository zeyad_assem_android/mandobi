package com.mandobiapp.driver.ui.activities.driver.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mandobiapp.driver.services.UpdatingDriverLocationService;
import com.squareup.picasso.Picasso;

import com.mandobiapp.driver.Gdata;
import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.ui.activities.base_activities.BaseAppCompatActivity;
import com.mandobiapp.driver.ui.activities.uiUitilities.RobotoTextView;

public class DriverProfileActivity extends BaseAppCompatActivity implements View.OnClickListener{
    ImageView driverImage;
    Button availableButton, unavailable_button;
    RobotoTextView driverName, address, telephoneNumber, email, vehicleColor, licenceNumber,vehicleSize;
    AsyncHttpClient client ;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_profile);
        context = this;
        initViews();
        bindData();

    }

    private void initViews(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        driverImage = (ImageView)findViewById(R.id.driver_image);
        availableButton = (Button)findViewById(R.id.available_button);
        availableButton.setOnClickListener(this);
        unavailable_button = (Button)findViewById(R.id.unavailable_button);
        unavailable_button.setOnClickListener(this);
        driverName = (RobotoTextView)findViewById(R.id.driver_name);
        address = (RobotoTextView)findViewById(R.id.address);
        telephoneNumber = (RobotoTextView)findViewById(R.id.telephone_number);
        email = (RobotoTextView)findViewById(R.id.email);
        vehicleColor = (RobotoTextView)findViewById(R.id.vehicle_color);
        licenceNumber = (RobotoTextView)findViewById(R.id.licence_number);
        vehicleSize = (RobotoTextView)findViewById(R.id.vehicle_size);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DriverEditPassword.class);
                startActivity(intent);
            }
        });
    }

    private void bindData(){
        client = new AsyncHttpClient();
        Picasso.with(this).load(URLConstants.IMAGES_BASE_URL + Gdata.photo).placeholder(R.drawable.avatar_placeholder).into(driverImage);
        if(Gdata.isbusy == "0")
            unavailable_button.setVisibility(View.VISIBLE);
        else
            availableButton.setVisibility(View.VISIBLE);

        driverName.setText(Gdata.name);
        address.setText(Gdata.address);
        telephoneNumber.setText(Gdata.phoneNumber);
        email.setText(Gdata.email);

        vehicleColor.setText(Gdata.vcolor);
        licenceNumber.setText(Gdata.plateNumber);
        vehicleSize.setText(getVehicleSize());
    }

    private String getVehicleSize(){
        if (Gdata.taxitype.equals("1")) {
            return "Small vehicle size";
        } else if (Gdata.taxitype.equals("2")) {
            return "Medium vehicle size";
        } else {
            return "Large vehicle size";
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        final Context context = this;
        switch (view.getId()){
            case R.id.available_button:
                RequestParams requestParams1 = new RequestParams();
                requestParams1.put("is_busy","1");
                client.get(URLConstants.BASE_URL+"w_m_s/update_user_is_busy.php?member_id="+Gdata.id,requestParams1,new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(String s) {
                        Gdata.isbusy = "0";
                        Toast.makeText(context,R.string.available_message,Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, UpdatingDriverLocationService.class);
                        intent.putExtra("driver_id", Gdata.id);
                        context.startService(intent);
                        unavailable_button.setVisibility(View.VISIBLE);
                        availableButton.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        Toast.makeText(context, R.string.something_wrong_message,Toast.LENGTH_LONG).show();
                    }
                });
                break;

            case R.id.unavailable_button:
                RequestParams requestParams = new RequestParams();
                requestParams.put("is_busy","0");
                client.post(URLConstants.BASE_URL+"w_m_s/update_user_is_busy.php?member_id="+Gdata.id,requestParams, new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(String s) {
                        Gdata.isbusy = "1";
                        Toast.makeText(context,R.string.unavailable_message,Toast.LENGTH_LONG).show();
                        availableButton.setVisibility(View.VISIBLE);
                        unavailable_button.setVisibility(View.INVISIBLE);
                    }
                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        Toast.makeText(context, R.string.something_wrong_message,Toast.LENGTH_LONG).show();
                    }
                });
                break;

        }

    }
}
