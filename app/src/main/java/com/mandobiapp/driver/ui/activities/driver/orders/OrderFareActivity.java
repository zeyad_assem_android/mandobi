package com.mandobiapp.driver.ui.activities.driver.orders;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import com.mandobiapp.driver.Gdata;
import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.ui.activities.driver.main.DriverMainActivity;

public class OrderFareActivity extends AppCompatActivity implements View.OnClickListener{
    EditText fare;
    Button submitButton;
    Bundle intentData;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_fare);
        context = this;
        initViews();
        getIntentData();
    }

    private void initViews() {
//        final Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
//        setSupportActionBar(toolbar);
//        initActionBar();
        fare = (EditText)findViewById(R.id.fare);
        fare.getBackground().setColorFilter(getResources().getColor(R.color.gery), PorterDuff.Mode.SRC_IN);
        submitButton = (Button)findViewById(R.id.submit_button);
        submitButton.setOnClickListener(this);
    }

    private void initActionBar() {
        final ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void getIntentData() {
        this.intentData = getIntent().getExtras();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit_button:
                AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                RequestParams requestParams = new RequestParams();
                requestParams.put("sender_id", intentData.getString("sender_id"));
                requestParams.put("reciever_id",intentData.getString("reciever_id"));
                requestParams.put("msg_id",intentData.getString("msg_id"));
                requestParams.put("price_move",fare.getText().toString());

                asyncHttpClient.post(URLConstants.BASE_URL+"w_m_s/reply_search1.php?lang=1",requestParams,new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(String s) {
                        Toast.makeText(context,R.string.notification_sent,Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, DriverMainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();

                    }

                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        Toast.makeText(context,R.string.something_wrong_message,Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(context, DriverMainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                        finish();
                    }
                });
        }
    }
}
