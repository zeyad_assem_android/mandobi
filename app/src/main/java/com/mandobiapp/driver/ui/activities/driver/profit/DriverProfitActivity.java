package com.mandobiapp.driver.ui.activities.driver.profit;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.mandobiapp.driver.Gdata;

import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.ui.activities.base_activities.BaseAppCompatActivity;
import com.mandobiapp.driver.ui.activities.uiUitilities.RobotoTextView;

public class DriverProfitActivity extends BaseAppCompatActivity implements View.OnClickListener{
    RobotoTextView dateTextView, change, totalOrdersNumber, totalPrice;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_profit);
        context = this;
        initViews();
        getData();
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        dateTextView = (RobotoTextView)findViewById(R.id.date_text_view);
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        SimpleDateFormat currentDate =new SimpleDateFormat("dd-MM-yyyy",Locale.ENGLISH);
        Date todayDate = new Date();
        String thisDate = currentDate.format(todayDate);
        dateTextView.setText(thisDate);
        change = (RobotoTextView)findViewById(R.id.change);
        change.setOnClickListener(this);
        totalOrdersNumber = (RobotoTextView)findViewById(R.id.total_order_number);
        totalPrice = (RobotoTextView)findViewById(R.id.total_price);
    }

    public void getData() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(String.format(URLConstants.BASE_URL+"w_m_s/w_profits.php?member_id=%s&t=6&type=recieve&u_t=w&date=%s", Gdata.id, dateTextView.getText()), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                try {
                    JSONObject j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        totalOrdersNumber.setText(j.getString("count"));
                        totalPrice.setText(j.getString("profits") + " $");
                    } else {
                        totalOrdersNumber.setText("0");
                        totalPrice.setText("0 $");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.change:
                new DatePickerDialog(context,R.style.MyDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        dateTextView.setText(simpleDateFormat.format(newDate.getTime()));
                        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
                        asyncHttpClient.get(String.format(URLConstants.BASE_URL+"w_m_s/w_profits.php?member_id=%s&t=6&type=recieve&u_t=w&date=%s", Gdata.id, simpleDateFormat.format(newDate.getTime())), new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(String s) {
                                try {
                                    JSONObject j = new JSONObject(s);
                                    if (j.getString("result").equals("true")) {
                                        totalOrdersNumber.setText(j.getString("count"));
                                        totalPrice.setText(j.getString("profits") + " $");
                                    } else {
                                        totalOrdersNumber.setText("0");
                                        totalPrice.setText("0 $");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
