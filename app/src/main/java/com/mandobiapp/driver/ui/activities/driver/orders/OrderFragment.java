package com.mandobiapp.driver.ui.activities.driver.orders;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mandobiapp.driver.Gdata;
import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.order;


/**
 * Created by Zeyad Assem on 20/09/16.
 */
public class OrderFragment extends Fragment {
    private int position;
    LinearLayout container;
    @NonNull
    public static OrderFragment newInstance(int position) {
        OrderFragment f = new OrderFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        this.position = getArguments().getInt("position");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_fragment, container, false);
        this.container = (LinearLayout) view.findViewById(R.id.container);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String url = URLConstants.BASE_URL+"c_m_s/get_user_orders.php";
        RequestParams requestParams = new RequestParams();
        requestParams.put("member_id", Gdata.id);
        requestParams.put("user_type","w");
        if(position == 0)
            requestParams.put("order_type","2");
        else if(position==1)
            requestParams.put("order_type", "6");
        else if(position ==2)
            requestParams.put("order_type","3");

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(url,requestParams,new AsyncHttpResponseHandler(){
            @Override
            public void onSuccess(String s) {
                JSONObject response = null;
                JSONArray orders = null;
                JSONObject order = null;
                try {
                    response = new JSONObject(s);
                    orders = response.getJSONArray("data");
                    if(orders.length() >0){
                        for (int i=0; i<orders.length();i++){
                            order = orders.getJSONObject(i);
                            order orderDataModel = new order();
                            orderDataModel.id = order.getString("order_id");
                            orderDataModel.name = order.getString("user_name");
                            orderDataModel.date = order.getString("order_date");
                            orderDataModel.from = order.getString("order_from_address");
                            orderDataModel.to = order.getString("order_to_address");
                            OrderItemViewModel orderItemViewModel = new OrderItemViewModel(getContext());
                            orderItemViewModel.bindData(orderDataModel);
                            orderItemViewModel.makeContainerClickable();
                            addToContainer(orderItemViewModel);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
                Toast.makeText(getContext(),"Something went wrong",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void addToContainer(OrderItemViewModel orderItemViewModel){
        if(this.container!=null)
        this.container.addView(orderItemViewModel);
    }
}
