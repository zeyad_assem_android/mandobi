package com.mandobiapp.driver.ui.activities.driver.orders.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.mandobiapp.driver.ui.activities.driver.orders.OrderFragment;

/**
 * Created by Zeyad Assem on 20/09/16.
 */
public class MyOrdersAdapter extends FragmentStatePagerAdapter {
    public MyOrdersAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return OrderFragment.newInstance(position);
    }


    @Override
    public int getCount() {
        return 3;
    }
}
