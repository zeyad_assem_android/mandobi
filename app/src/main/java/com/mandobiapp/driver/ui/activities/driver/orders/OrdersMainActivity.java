package com.mandobiapp.driver.ui.activities.driver.orders;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.mandobiapp.driver.R;
import com.mandobiapp.driver.ui.activities.base_activities.BaseAppCompatActivity;
import com.mandobiapp.driver.ui.activities.driver.orders.adapters.MyOrdersAdapter;

import java.util.Locale;

/**
 * Created by Zeyad Assem on 17/09/16.
 */
public class OrdersMainActivity extends BaseAppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MyOrdersAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_main);
        //Setting the tool bar
        final Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.actionBarToolBar);
        setSupportActionBar(actionBarToolbar);
        initActionBar();

        tabLayout = (TabLayout) findViewById(R.id.tabLayout); //initialize the tab layout
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        setTabs();

        adapter = new MyOrdersAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(adapter);
//        viewPager.setCurrentItem(0);

        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        changeTabsFont();

    }

    private void initActionBar() {
        final ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
            mActionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    private void setTabs() {
        tabLayout.addTab(tabLayout.newTab().setContentDescription(R.string.open_orders).setText(R.string.open_orders));

        tabLayout.addTab(tabLayout.newTab().setContentDescription(R.string.executed_orders).setText(R.string.executed_orders));

        tabLayout.addTab(tabLayout.newTab().setContentDescription(R.string.canceled_orders).setText(R.string.canceled_orders));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    setMediumTextFont(((TextView) tabViewChild), this);
                }
            }
        }
    }

    public static void setMediumTextFont(@Nullable TextView textView, @NonNull Context context) {


        Typeface font = getCustomFont("fonts/Roboto-Regular.ttf", context);
        if (font != null && textView != null){
            textView.setTypeface(font);
            textView.setTextColor(context.getResources().getColor(R.color.white));
        }


    }

    private static Typeface getCustomFont(String fontPath, @NonNull Context context) {
        ArrayMap<String, Typeface> cacheFont = new ArrayMap<>();
        if (cacheFont == null)
            cacheFont = new ArrayMap<>();

        if (cacheFont.containsKey(fontPath)) {
            return cacheFont.get(fontPath);

        } else {
            Typeface loadedFont = Typeface.createFromAsset(context.getAssets(),
                    fontPath);
            cacheFont.put(fontPath, loadedFont);
        }

        return cacheFont.get(fontPath);
    }
}
