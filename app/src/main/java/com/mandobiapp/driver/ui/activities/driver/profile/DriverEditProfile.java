package com.mandobiapp.driver.ui.activities.driver.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mandobiapp.driver.Gdata;
import com.mandobiapp.driver.LoginActivity;
import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.ui.activities.MainActivity;
import com.mandobiapp.driver.ui.activities.base_activities.BaseAppCompatActivity;
import com.mandobiapp.driver.ui.activities.driver.main.DriverMainActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class DriverEditProfile extends BaseAppCompatActivity implements View.OnClickListener{
    Toolbar toolbar;
    EditText driverName, address, telephoneNumber, email, vehicleColor, licenceNumber;
    Spinner vehicleSize;
    Context context;
    ProgressDialog progressDialog;
    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_edit_profile);
        context = this;
        initViews();
    }

    private void initViews() {
        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        driverName = (EditText) findViewById(R.id.driver_name);
        driverName.setText(Gdata.name);
        address = (EditText)findViewById(R.id.address);
        address.setText(Gdata.address);
        telephoneNumber = (EditText)findViewById(R.id.telephone_number);
        telephoneNumber.setText(Gdata.phoneNumber);
        email = (EditText)findViewById(R.id.email);
        email.setText(Gdata.email);
        vehicleColor = (EditText)findViewById(R.id.vehicle_color);
        vehicleColor.setText(Gdata.vcolor);
        licenceNumber = (EditText)findViewById(R.id.licence_number);
        licenceNumber.setText(Gdata.plateNumber);
        vehicleSize = (Spinner)findViewById(R.id.vehicle_size);
        submit = (Button)findViewById(R.id.submit);
        submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        progressDialog = ProgressDialog.show(context, getString(R.string.wait), getString(R.string.load), true);
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();
        requestParams.put("driver_id",Gdata.id);
        if(! driverName.getText().toString().isEmpty())
            requestParams.put("driver_name",driverName.getText().toString());
        if(! address.getText().toString().isEmpty())
            requestParams.put("driver_address",address.getText().toString());
        if (! telephoneNumber.getText().toString().isEmpty())
            requestParams.put("driver_telephone_number",telephoneNumber.getText().toString());
        if(! email.getText().toString().isEmpty())
            requestParams.put("driver_email",email.getText().toString());
        if(! vehicleColor.getText().toString().isEmpty())
            requestParams.put("driver_vehicle_color",vehicleColor.getText().toString());
        if(!licenceNumber.getText().toString().isEmpty())
            requestParams.put("driver_licence_number",licenceNumber.getText().toString());
        if(!vehicleSize.getSelectedItem().toString().isEmpty()){
            String type = "1";
            if(vehicleSize.getSelectedItemPosition() ==0){
                type = "1";
            }
            if(vehicleSize.getSelectedItemPosition() ==1){
                type = "2";
            }
            if(vehicleSize.getSelectedItemPosition() ==2){
                type = "3";
            }
            requestParams.put("driver_vehicle_size",type);
        }
        if(Gdata.emailValidator(email.getText().toString())){
            client.post(URLConstants.BASE_URL+"w_m_s/update_profile.php",requestParams, new AsyncHttpResponseHandler(){
                @Override
                public void onSuccess(String s) {
                    try {
                        JSONObject response = new JSONObject(s);
                        if(response.getString("result").equals("false")){
                            progressDialog.dismiss();
                            Toast.makeText(context,response.getString("data"),Toast.LENGTH_SHORT).show();
                        }else{
                            progressDialog.dismiss();
                            Toast.makeText(context,R.string.edit_profile_done,Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(context, DriverMainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                        }
                    } catch (JSONException e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Throwable throwable, String s) {
                    progressDialog.dismiss();
                    Toast.makeText(context,R.string.something_wrong_message, Toast.LENGTH_LONG).show();
                }
            });
        }else{
            progressDialog.dismiss();
            Toast.makeText(context,R.string.review_email,Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
