package com.mandobiapp.driver.ui.activities.base_activities;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.managers.SharedPreferenceManager;
import com.mandobiapp.driver.ui.activities.driver.orders.OrderDetails;

import java.util.Locale;

/**
 * Created by Zeyad Assem on 21/09/16.
 */

public class BaseAppCompatActivity extends AppCompatActivity {
    public static BroadcastReceiver broadcastReceiver;
    public static IntentFilter intentFilter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLanguage();
        changeStatusBarColor();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setLanguage();
        if(broadcastReceiver ==null){
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Intent orderIntent = new Intent(context, OrderDetails.class);
                    orderIntent.putExtra("client_id", intent.getStringExtra("client_id"));
                    orderIntent.putExtra("msg_id", intent.getStringExtra("msg_id"));
                    orderIntent.putExtra("type",intent.getStringExtra("type"));
                    orderIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(orderIntent);
                }
            };
            intentFilter = new IntentFilter("OPEN_NEW_ACTIVITY");
        }
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(broadcastReceiver != null)
            unregisterReceiver(broadcastReceiver);
        broadcastReceiver = null;
    }

    private void setLanguage(){
        if(!SharedPreferenceManager.getInstance(this).getCurrentLanguage().equals("default")){
            String languageToLoad  = SharedPreferenceManager.getInstance(this).getCurrentLanguage(); // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
        }

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor(){
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(this.getResources().getColor(R.color.black));
    }

}
