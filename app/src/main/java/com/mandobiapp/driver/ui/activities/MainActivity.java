package com.mandobiapp.driver.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.mandobiapp.driver.ClientloginActivity;
import com.mandobiapp.driver.Gdata;
import com.mandobiapp.driver.LoginActivity;
import com.mandobiapp.driver.R;


public class MainActivity extends Activity implements View.OnClickListener{
    LinearLayout clientLogin, driverLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        Gdata.backlist.add("main");
        Gdata.context="m";
    }

    private void initViews(){
        clientLogin = (LinearLayout) findViewById(R.id.client_login);
        driverLogin = (LinearLayout) findViewById(R.id.driver_login);

        clientLogin.setOnClickListener(this);
        driverLogin.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.client_login:
                intent = new Intent(MainActivity.this, ClientloginActivity.class);
                Gdata.backlist.add("m");
                startActivity(intent);
                finish();
                break;
            case R.id.driver_login:
                intent= new Intent(MainActivity.this, LoginActivity.class);
                Gdata.backlist.add("m");
                startActivity(intent);
                finish();
                break;

        }
    }
}
