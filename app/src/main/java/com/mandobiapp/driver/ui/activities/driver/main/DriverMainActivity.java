package com.mandobiapp.driver.ui.activities.driver.main;

import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.Manifest;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import com.mandobiapp.driver.Gdata;
import com.mandobiapp.driver.R;
import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.ui.activities.base_activities.BaseAppCompatActivity;
import com.mandobiapp.driver.utilities.Logger;

/**
 * Created by Zeyad Assem on 02/09/16.
 */
public class DriverMainActivity extends BaseAppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private Toolbar toolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private MapFragment mapFragment;
    private double longitude, latitude;
    private GoogleMap googleMap;
    private ImageView currentLocationButton, showTraffic;
    private boolean isTrafficEnabled = false;
    private MarkerOptions markerOptions;
    private final int ACCESS_FINE_LOCATION_REQUEST_CODE = 0;
    private final int ACCESS_COARSE_LOCATION_REQUEST_CODE = 1;
    private boolean isAccessFineLocationEnabled, isAccessCoarseLocationEnabled;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_main);
        initViews();
        sendTokenToServer();
    }

    private void sendTokenToServer() {
        String token = FirebaseInstanceId.getInstance().getToken();
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();
        requestParams.put("member_id", Gdata.id);
        requestParams.put("access_key", token);
        client.post(URLConstants.BASE_URL+"w_m_s/insert_access_key.php", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                Logger.LOG_I("token success", s);
            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
                Logger.LOG_I("token failure", s);
            }
        });
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        initActionBar();
        setNavigationDrawer();
        currentLocationButton = (ImageView) findViewById(R.id.current_location_button);
        currentLocationButton.setOnClickListener(this);
        showTraffic = (ImageView) findViewById(R.id.traffic);
        showTraffic.setOnClickListener(this);
        initMapFragment();
    }

    private void initGoogleAPIClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    private void initMapFragment() {
        mapFragment = MapFragment.newInstance();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.map_frame, mapFragment);
        fragmentTransaction.commit();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                mapFragment.getMapAsync(this);
            }else{
                requestCoarseLocation();
            }
        } else {
            requestFineLocation();
        }
        mapFragment.getMapAsync(this);

    }

    private void initActionBar() {
        final ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME);
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setNavigationDrawer() {
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment1, SideMenuFragment.newInstance(), "side_menu").commit();
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        initGoogleAPIClient();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                this.googleMap.setMyLocationEnabled(true);
            }else{
                requestCoarseLocation();
            }
        } else {
            requestFineLocation();
        }
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation != null) {
            latitude = lastLocation.getLatitude();
            longitude = lastLocation.getLongitude();
        }
        LatLng currentLocation = new LatLng(latitude, longitude);
        if (markerOptions == null) {
            markerOptions = new MarkerOptions().position(currentLocation).title("Current location").icon(BitmapDescriptorFactory.fromResource(R.drawable.mypin));
        } else {
            markerOptions.position(currentLocation);
        }
        googleMap.clear();
        googleMap.addMarker(markerOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15f));
        sendLocationToServer(latitude, longitude);
    }

    private void sendLocationToServer(double latitude, double longitude) {
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        RequestParams requestParams = new RequestParams();
        requestParams.put("driver_id",Gdata.id);
        requestParams.put("driver_longitude", Double.toString(longitude));
        requestParams.put("driver_latitude", Double.toString(latitude));
        asyncHttpClient.post(URLConstants.BASE_URL+"w_m_s/update_driver_location.php",requestParams, new AsyncHttpResponseHandler(){
            @Override
            public void onSuccess(String s) {
                Logger.LOG_I("response - success",s);
            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                Logger.LOG_I("response - failed" , s);
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onStart() {
        if (googleApiClient != null)
            googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (googleApiClient != null)
            googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.current_location_button:
                if (mapFragment != null)
                    mapFragment.getMapAsync(this);
                break;

            case R.id.traffic:
                if (googleMap != null) {
                    if (isTrafficEnabled) {
                        googleMap.setTrafficEnabled(false);
                        isTrafficEnabled = false;
                    } else {
                        googleMap.setTrafficEnabled(true);
                        isTrafficEnabled = true;
                    }
                }
                break;
        }
    }

    private void requestFineLocation() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)) {
            Toast.makeText(this,"we need to access your location",Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},ACCESS_FINE_LOCATION_REQUEST_CODE);
        }
    }

    private void requestCoarseLocation() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_COARSE_LOCATION)) {
            Toast.makeText(this,"we need to access your location",Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},ACCESS_COARSE_LOCATION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == ACCESS_FINE_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                    this.googleMap.setMyLocationEnabled(true);
                }else{
                    requestCoarseLocation();
                }
            } else {
                Toast.makeText(this,"We wouldn't be able to access your location",Toast.LENGTH_LONG);
            }
        }else if (requestCode == ACCESS_COARSE_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 && permissions[0] == Manifest.permission.ACCESS_COARSE_LOCATION &&grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                    this.googleMap.setMyLocationEnabled(true);
                }else{
                    requestFineLocation();
                }
            } else {
                Toast.makeText(this,"We wouldn't be able to access your location",Toast.LENGTH_LONG);
            }
        }
    }
}
