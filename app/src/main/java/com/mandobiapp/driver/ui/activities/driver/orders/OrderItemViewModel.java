package com.mandobiapp.driver.ui.activities.driver.orders;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.mandobiapp.driver.R;
import com.mandobiapp.driver.order;
import com.mandobiapp.driver.ui.activities.uiUitilities.RobotoTextView;

/**
 * Created by Zeyad Assem on 20/09/16.
 */
public class OrderItemViewModel extends LinearLayout implements View.OnClickListener{
    LinearLayout container;
    RobotoTextView clientName, orderDate, fromAddress, toAddress;
    order order;
    boolean isContainerClickable = false;
    public OrderItemViewModel(Context context) {
        this(context, null);
    }

    public OrderItemViewModel(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public OrderItemViewModel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        inflate(getContext(), R.layout.order_item, this);
        container = (LinearLayout)findViewById(R.id.container);
        container.setOnClickListener(this);
        clientName = (RobotoTextView)findViewById(R.id.client_name);
        orderDate = (RobotoTextView)findViewById(R.id.order_date);
        fromAddress = (RobotoTextView)findViewById(R.id.pick_up_location);
        toAddress = (RobotoTextView)findViewById(R.id.pick_off_location);
    }

    public void bindData(order order){
        this.order = order;
        this.clientName.setText(order.name);
        this.orderDate.setText(order.date);
        this.fromAddress.setText(order.from);
        this.toAddress.setText(order.to);
    }

    public void makeContainerClickable(){
        this.isContainerClickable = true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.container:
                if(isContainerClickable){
                    Intent intent = new Intent(getContext(),OrderDetails.class);
                    intent.putExtra("msg_id", order.id);
                    intent.putExtra("type","details");
                    getContext().startActivity(intent);
                }
                break;
        }
    }
}
