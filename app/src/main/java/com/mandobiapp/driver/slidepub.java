package com.mandobiapp.driver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import java.util.Locale;

/**
 * Created by elkholy on 23/12/2015.
 */
public  class slidepub {
    public static SlidingMenu mLMenu;

    public static void slidemenu(final Context context) {
        mLMenu = new SlidingMenu(context);
        if (Locale.getDefault().getLanguage().equals("ar")){
            mLMenu.setMode(SlidingMenu.RIGHT);
        }else {
        mLMenu.setMode(SlidingMenu.LEFT);
        }
        mLMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        mLMenu.setShadowWidthRes(R.dimen.shadow_width);
        mLMenu.setShadowDrawable(R.drawable.shadow);
        mLMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        mLMenu.setFadeDegree(0.45f);
        mLMenu.attachToActivity(((Activity) context)
                , SlidingMenu.SLIDING_CONTENT);
        mLMenu.setMenu(R.layout.menupub);
        final LinearLayout lin = (LinearLayout) mLMenu.findViewById(R.id.lin);
        final LinearLayout lin1 = (LinearLayout) mLMenu.findViewById(R.id.lin1);
        final LinearLayout lin2 = (LinearLayout) mLMenu.findViewById(R.id.lin2);
        final LinearLayout lin3 = (LinearLayout) mLMenu.findViewById(R.id.lin3);
        final LinearLayout lin4 = (LinearLayout) mLMenu.findViewById(R.id.lin4);
        final LinearLayout lin5 = (LinearLayout) mLMenu.findViewById(R.id.lin5);
        final LinearLayout lin6 = (LinearLayout) mLMenu.findViewById(R.id.lin6);
        final LinearLayout lin7 = (LinearLayout) mLMenu.findViewById(R.id.lin7);
        final TextView menu = (TextView) ((Activity) context).findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLMenu.showMenu(true);
            }
        });
        lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("cl")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, ClientloginActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();

            }
        });
        lin1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("cr")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, ClientregActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();

            }
        });
        lin2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("ml")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, LoginActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();
            }
        });
        lin3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("mr")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, RegisterActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();

            }
        });
        lin4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("ma")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, About.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();

            }
        });
        lin5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("mho")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, howtoregister.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();

            }
        });
        lin6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("mpo")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, policy.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();

            }
        });
        lin7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!Gdata.context.equals("cp")) {
                Gdata.backlist.add(Gdata.context);
//                }
                Intent i = new Intent(context, ChangeActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();

            }
        });
    }


}
