package com.mandobiapp.driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Editprofile extends Activity {
    TextView  small, lsmall, medium, lmedium, large, llarge, reg;
    EditText name, username, password, cpassword, mobile, email, vcolor, dln,address;
    String file, type;
    ProgressBar progress;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        small = (TextView) findViewById(R.id.small);
        lsmall = (TextView) findViewById(R.id.csmall);
        medium = (TextView) findViewById(R.id.medium);
        lmedium = (TextView) findViewById(R.id.cmedium);
        large = (TextView) findViewById(R.id.large);
        llarge = (TextView) findViewById(R.id.clarge);
        reg = (TextView) findViewById(R.id.reg);
        name = (EditText) findViewById(R.id.name);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        cpassword = (EditText) findViewById(R.id.cpassword);
        mobile = (EditText) findViewById(R.id.mobile);
        email = (EditText) findViewById(R.id.email);
        vcolor = (EditText) findViewById(R.id.vcolor);
        dln = (EditText) findViewById(R.id.dln);
        address = (EditText) findViewById(R.id.address);
        progress = (ProgressBar)findViewById(R.id.progress);
        slidemandob.slidemenu(Editprofile.this);
        small.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "1";
                lsmall.setVisibility(View.VISIBLE);
                lmedium.setVisibility(View.INVISIBLE);
                llarge.setVisibility(View.INVISIBLE);
            }
        });
        medium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "2";
                lmedium.setVisibility(View.VISIBLE);
                lsmall.setVisibility(View.INVISIBLE);
                llarge.setVisibility(View.INVISIBLE);
            }
        });
        large.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                type = "3";
                llarge.setVisibility(View.VISIBLE);
                lmedium.setVisibility(View.INVISIBLE);
                lsmall.setVisibility(View.INVISIBLE);
            }
        });
        Toast t = Toast.makeText(Editprofile.this, getString(R.string.passwordletter), Toast.LENGTH_LONG);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager
                        .getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    if (username.getText().toString().equals("") || username.getText().toString().replaceAll(" ", "").equals("") || address.getText().toString().equals("") || address.getText().toString().replaceAll(" ", "").equals("") || name.getText().toString().equals("") || name.getText().toString().replaceAll(" ", "").equals("") || mobile.getText().toString().equals("") || mobile.getText().toString().replaceAll(" ", "").equals("") || vcolor.getText().toString().equals("") || vcolor.getText().toString().replaceAll(" ", "").equals("") || dln.getText().toString().equals("") || dln.getText().toString().replaceAll(" ", "").equals("") || type == null) {
                        Toast t = Toast.makeText(Editprofile.this, getString(R.string.complete), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else if (password.getText().toString().length() < 7 && password.getText().toString().length() != 0) {
                        Toast t = Toast.makeText(Editprofile.this, getString(R.string.passwordu), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else if (!password.getText().toString().equals(cpassword.getText().toString()) && password.getText().toString().length() > 6) {
                        Toast t = Toast.makeText(Editprofile.this, getString(R.string.pnot), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else if (Gdata.emailValidator(email.getText().toString()) == false || email.getText().toString().equals("") || email.getText().toString().replaceAll(" ", "").equals("")) {
                        Toast t = Toast.makeText(Editprofile.this, getString(R.string.emailc), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else {
                        new GetOrders().execute();
//                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
//                                .permitAll().build();
//                        StrictMode.setThreadPolicy(policy);
//                        try {
//                            progress.setVisibility(View.VISIBLE);
//                            String m = h("http://mandobiapp.com/w_m_s/update_profile.php", Gdata.id, name.getText().toString(), username.getText().toString(), password.getText().toString(), email.getText().toString(), mobile.getText().toString(), address.getText().toString(), vcolor.getText().toString(), dln.getText().toString(), type);
//                            JSONObject j = new JSONObject(m);
//                            if (j.getString("result").equals("true")) {
//                                progress.setVisibility(View.INVISIBLE);
//                                Intent i = new Intent(Editprofile.this, ProfileActivity.class);
//                                Gdata.name=name.getText().toString();
//                                Gdata.username=username.getText().toString();
//                                Gdata.phoneNumber=mobile.getText().toString();
//                                Gdata.address=address.getText().toString();
//                                Gdata.email=email.getText().toString();
//                                Gdata.vcolor=vcolor.getText().toString();
//                                Gdata.taxitype=type;
//                                Gdata.plateNumber=dln.getText().toString();
//                                startActivity(i);
//                                finish();
//                            } else {
//                                progress.setVisibility(View.INVISIBLE);
//                                Toast t = Toast.makeText(Editprofile.this, j.getString("data"), Toast.LENGTH_LONG);
//                                t.setGravity(Gravity.CENTER, 0, 0);
//                                t.show();
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }

                }
            }


        });
        type = Gdata.taxitype;
        name.setText(Gdata.name);
        username.setText(Gdata.username);
        address.setText(Gdata.address);
        mobile.setText(Gdata.phoneNumber);
        email.setText(Gdata.email);
        vcolor.setText(Gdata.vcolor);
        dln.setText(Gdata.plateNumber);
        if (Gdata.taxitype.equals(getString(R.string.small))){
            lsmall.setVisibility(View.VISIBLE);
        }else  if (Gdata.taxitype.equals(getString(R.string.medium))){
            lmedium.setVisibility(View.VISIBLE);
        }else  if (Gdata.taxitype.equals(getString(R.string.large))){
            llarge.setVisibility(View.VISIBLE);
        }
    }
    public  String h(String url,String id, String f_name, String username, String password, String email, String phon, String place, String vcolor,String dln ,String type)  {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("f_name", new StringBody(f_name));
            multipartEntity.addPart("member_id",new StringBody(id));
            multipartEntity.addPart("username", new StringBody(username));
            multipartEntity.addPart("password", new StringBody(password));
            multipartEntity.addPart("email", new StringBody(email));
            multipartEntity.addPart("mobile", new StringBody(phon));
            multipartEntity.addPart("plate_number", new StringBody(dln));
            multipartEntity.addPart("taxi_type", new StringBody(type));
            multipartEntity.addPart("place", new StringBody(place));
            multipartEntity.addPart("color",new StringBody(vcolor));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
                Looper.prepare();
                progressDialog.dismiss();
                Toast t = Toast.makeText(Editprofile.this, getString(R.string.check), Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                Looper.loop();
            }

        return json;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Editprofile.this,ProfileActivity.class);
        startActivity(i);
        finish();
    }

    class GetOrders extends AsyncTask<Void, String, String> {
        String s;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(Editprofile.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = h("http://mandobiapp.com/w_m_s/update_profile.php", Gdata.id, name.getText().toString(), username.getText().toString(), password.getText().toString(), email.getText().toString(), mobile.getText().toString(), address.getText().toString(), vcolor.getText().toString(), dln.getText().toString(), type);
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        progress.setVisibility(View.INVISIBLE);
                        Intent i = new Intent(Editprofile.this, ProfileActivity.class);
                        Gdata.name=name.getText().toString();
                        Gdata.username=username.getText().toString();
                        Gdata.phoneNumber =mobile.getText().toString();
                        Gdata.address=address.getText().toString();
                        Gdata.email=email.getText().toString();
                        Gdata.vcolor=vcolor.getText().toString();
                        Gdata.taxitype=type;
                        Gdata.plateNumber =dln.getText().toString();
                        startActivity(i);
                        finish();
                    } else {
                        progress.setVisibility(View.INVISIBLE);
                        Toast t = Toast.makeText(Editprofile.this, j.getString("data"), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }

    }

}
