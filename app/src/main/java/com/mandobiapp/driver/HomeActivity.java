package com.mandobiapp.driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class HomeActivity extends Activity {
    TextView order, profile, logout,profits;
    ProgressDialog progress;
    SharedPreferences storedata;
    private static String filename = "mlogin";
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        order = (TextView) findViewById(R.id.order);
        profile = (TextView) findViewById(R.id.profile);
        logout = (TextView) findViewById(R.id.logout);
        profits = (TextView) findViewById(R.id.profits);
      slidemandob.slidemenu(HomeActivity.this);
        Gdata.context="mh";
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, OrderActivity.class);
                Gdata.backlist.add("mh");
                startActivity(i);
                finish();
            }
        });
        profits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, ProfitsActivity.class);
                Gdata.backlist.add("mh");
                startActivity(i);
                finish();
            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, ProfileActivity.class);
                Gdata.backlist.add("mh");
                startActivity(i);
                finish();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager
                        .getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    new GetOrders().execute();
//                    try {
//                        String m = h("http://mandobiapp.com/w_m_s/logout_update.php", Gdata.id);
//                        JSONObject jo = new JSONObject(m);
//                        if (jo.getString("result").equals("true")) {
//                            storedata = getSharedPreferences(filename, 0);
//                            storedata.edit().clear().commit();
//                            Intent i = new Intent(HomeActivity.this, LoginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            finish();
////                            Gdata.backlist.removeAll(Gdata.backlist);
////                            Gdata.backlist.add("home");
//                        } else {
//                            Toast.makeText(HomeActivity.this, jo.getString("data"), Toast.LENGTH_LONG).show();
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

                } else {
                    Looper.prepare();
                    Toast t = Toast.makeText(HomeActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                    Looper.loop();
                }
            }
        });
        GCMClientManager pushClientManager = new GCMClientManager(this, "829557468935");
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {

                Log.d("Registration id", registrationId);
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            try {
                    response = Gdata.h(HomeActivity.this, "http://mandobiapp.com/w_m_s/insert_access_key.php", registrationId, Gdata.id);
                if (response.contains("{\"result\":")) {
                    JSONObject j = new JSONObject(response);
                    if (j.getString("result").equals("true")) {

                    } else {
                    }
                }else {
                    Looper.prepare();
                    Toast t = Toast.makeText(HomeActivity.this, getString(R.string.sure), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                    Looper.loop();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }                }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
            }
        });
//        Intent i = new Intent("com.google.android.c2dm.intent.REGISTER");
//        i.putExtra("app", PendingIntent.getBroadcast(HomeActivity.this,
//                0, new Intent(), 0));
//        i.putExtra("sender", "829557468935");
//        startService(i);


//        Intent intent = new Intent(HomeActivity.this, MyIDListenerService.class);
//        startService(intent);
//        String deviceToken = null;
//        try {
//            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
//            deviceToken = gcm.register("829557468935");
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
//                    .permitAll().build();
//            StrictMode.setThreadPolicy(policy);
//            String response = null;
//            try {
//                    response = Gdata.h(HomeActivity.this, "http://mandobiapp.com/w_m_s/insert_access_key.php", deviceToken, Gdata.id);
//                if (response.contains("{\"result\":")) {
//                    JSONObject j = new JSONObject(response);
//                    if (j.getString("result").equals("true")) {
//
//                    } else {
//                    }
//                }else {
//                    Looper.prepare();
//                    Toast t = Toast.makeText(HomeActivity.this, getString(R.string.sure), Toast.LENGTH_LONG);
//                    t.setGravity(Gravity.CENTER, 0, 0);
//                    t.show();
//                    Looper.loop();
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Log.i("GCM", "Device token : " + deviceToken);

                // update user profile document


    }

    public  String h(String url, String id)  {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("member_id", new StringBody(id));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
            Looper.prepare();
            progressDialog.dismiss();
            Toast t = Toast.makeText(HomeActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            Looper.loop();
            }

        return json;
    }
    class GetOrders extends AsyncTask<Void, String, String> {
        String s;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(HomeActivity.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = h("http://mandobiapp.com/w_m_s/logout_update.php", Gdata.id);
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        storedata = getSharedPreferences(filename, 0);
                        storedata.edit().clear().commit();
                        Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
                        Gdata.backlist.removeAll(Gdata.backlist);
                        Gdata.count=0;
                        System.runFinalizersOnExit(true);
                        System.exit(0);
                        android.os.Process.killProcess(android.os.Process.myPid());
                    } else {
                        Toast.makeText(HomeActivity.this, j.getString("data"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }

    }
    @Override
    public void onBackPressed() {
       Gdata.back(HomeActivity.this);
    }
}
