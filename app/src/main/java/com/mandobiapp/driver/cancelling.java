package com.mandobiapp.driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Looper;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;


public class cancelling extends Activity {
    TextViewWithFont title, name, phone, accept, busy, taxitype;
    TextView call, menu;
    ImageView image;
    GoogleMap mmap;
    Marker marker;
    AsyncHttpClient client = new AsyncHttpClient();
    SharedPreferences storedata;
    private static String filename = "file";
    String username, pass;
    RadioGroup group;
    String s;
    LinearLayout lin;
    ProgressBar progress;
    ArrayList<String> list = new ArrayList<>();
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancelling);
        busy = (TextViewWithFont) findViewById(R.id.view);
        group = (RadioGroup) findViewById(R.id.groub);
        lin = (LinearLayout) findViewById(R.id.lin);
        final RadioButton r1 = (RadioButton) findViewById(R.id.r1);
        final RadioButton r2 = (RadioButton) findViewById(R.id.r2);
        final RadioButton r3 = (RadioButton) findViewById(R.id.r3);
        final RadioButton r4 = (RadioButton) findViewById(R.id.r4);
        final RadioButton r5 = (RadioButton) findViewById(R.id.r5);
        final RadioButton r6 = (RadioButton) findViewById(R.id.r6);
         slideclient.slidemenu(cancelling.this);
        progressDialog = ProgressDialog.show(cancelling.this, getString(R.string.wait), getString(R.string.load), true);
        client.get("http://mandobiapp.com/c_m_s/suggestion_massages.php", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                try {
                    list.removeAll(list);
                    JSONObject j = new JSONObject(s);
                    JSONArray jary = j.getJSONArray("data");
                    if (Locale.getDefault().getLanguage().equals("en")) {
                        r1.setText(jary.getJSONObject(0).getString("msg_en"));
                        r2.setText(jary.getJSONObject(1).getString("msg_en"));
                        r3.setText(jary.getJSONObject(2).getString("msg_en"));
                        r4.setText(jary.getJSONObject(3).getString("msg_en"));
                        r5.setText(jary.getJSONObject(4).getString("msg_en"));
                        r6.setText(jary.getJSONObject(5).getString("msg_en"));
                    }
                    else if (Locale.getDefault().getLanguage().equals("ar")) {
                        r1.setText(jary.getJSONObject(0).getString("msg_txt"));
                        r2.setText(jary.getJSONObject(1).getString("msg_txt"));
                        r3.setText(jary.getJSONObject(2).getString("msg_txt"));
                        r4.setText(jary.getJSONObject(3).getString("msg_txt"));
                        r5.setText(jary.getJSONObject(4).getString("msg_txt"));
                        r6.setText(jary.getJSONObject(5).getString("msg_txt"));
                    } else if (Locale.getDefault().getLanguage().equals("ur")) {
                        r1.setText(jary.getJSONObject(0).getString("msg_lang3"));
                        r2.setText(jary.getJSONObject(1).getString("msg_lang3"));
                        r3.setText(jary.getJSONObject(2).getString("msg_lang3"));
                        r4.setText(jary.getJSONObject(3).getString("msg_lang3"));
                        r5.setText(jary.getJSONObject(4).getString("msg_lang3"));
                        r6.setText(jary.getJSONObject(5).getString("msg_lang3"));
                    }
                    list.add(jary.getJSONObject(0).getString("massage_id"));
                    list.add(jary.getJSONObject(1).getString("massage_id"));
                    list.add(jary.getJSONObject(2).getString("massage_id"));
                    list.add(jary.getJSONObject(3).getString("massage_id"));
                    list.add(jary.getJSONObject(4).getString("massage_id"));
                    list.add(jary.getJSONObject(5).getString("massage_id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                lin.setVisibility(View.VISIBLE);
                progressDialog.dismiss();

            }
        });

        // String s = ((RadioButton) findViewById(group.getCheckedRadioButtonId())).getText().toString();
        busy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.equals(null)) {
                    Looper.prepare();
                    Toast t = Toast.makeText(cancelling.this, getString(R.string.write), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                    Looper.loop();
                } else {
                    progressDialog = ProgressDialog.show(cancelling.this, getString(R.string.wait), getString(R.string.load), true);
                    client.get(String.format("http://mandobiapp.com/c_m_s/re_reply_search_cancel.php?sender_id=%s&reciever_id=%s&msg_id=%s&refuse=%s&lang=1", Gdata.user_id, Gdata.id, Gdata.message_id,s), new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(String s) {
                            try {
                                JSONObject j = new JSONObject(s);
                                if (j.getString("result").equals("true")) {
                                    Intent i = new Intent(cancelling.this, ClientprofileActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Looper.prepare();
                                    Toast t = Toast.makeText(cancelling.this, j.getString("data"), Toast.LENGTH_LONG);
                                    t.setGravity(Gravity.CENTER, 0, 0);
                                    t.show();
                                    Looper.loop();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }
        });
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.r1) {
                    s = list.get(0);
                } else if (checkedId == R.id.r2) {
                    s = list.get(1);
                } else if (checkedId == R.id.r3) {
                    s = list.get(2);
                } else if (checkedId == R.id.r4) {
                    s = list.get(3);
                } else if (checkedId == R.id.r5) {
                    s = list.get(4);
                } else if (checkedId == R.id.r6) {
                    s = list.get(5);
                }
            }
        });
    }

}

