package com.mandobiapp.driver;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class clientorderAdapter extends ArrayAdapter<order> {
    private final LayoutInflater mInflater;
    Context context;
    SQLiteDatabase db1 = null;
    public static String ads = "Resourses_country.db";
    // AsyncHttpClient client;
    int x = 0;

    public clientorderAdapter(Context context, int resource) {
        super(context, resource);
        // TODO Auto-generated constructor stub
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        db1 = context.openOrCreateDatabase(ads, Context.MODE_PRIVATE, null);
        // client = new AsyncHttpClient();
    }

    private static class ViewHolder {
        @SuppressWarnings("unused")
        public TextView title;
        @SuppressWarnings("unused")
        public TextView from;
        @SuppressWarnings("unused")
        public TextView to;
        public TextView transport;
        public ImageView transimage;
        public LinearLayout header;
        public LinearLayout details;

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressLint({"ResourceAsColor", "NewApi"})
    @Override
    public View getView(final int position, View v, final ViewGroup parent) {
        ViewHolder holder = null;
        if (v == null)
            v = mInflater.inflate(R.layout.clientorder, null);
        else
            holder = (ViewHolder) v.getTag();

        final order news = this.getItem(position);
        if (news != null) {
            TextView title = (TextView) v.findViewById(R.id.name);
            TextView from = (TextView) v.findViewById(R.id.shipfrom);
            TextView to = (TextView) v.findViewById(R.id.shipto);
            TextView trans = (TextView) v.findViewById(R.id.trasport);
            ImageView transimage = (ImageView) v.findViewById(R.id.transimage);
            final LinearLayout header = (LinearLayout) v.findViewById(R.id.header);
            final LinearLayout details = (LinearLayout) v.findViewById(R.id.details);
            title.setText(news.title);
           from.setText(news.from);
            to.setText(news.to);
            if (news.type.equals("1")) {
                trans.setText(context.getString(R.string.small));
                transimage.setBackgroundResource(R.drawable.carsmall);
            } else if (news.type.equals("2")) {
                trans.setText(context.getString(R.string.medium));
                transimage.setBackgroundResource(R.drawable.carmedium);
            } else {
                    trans.setText(context.getString(R.string.large));
                    transimage.setBackgroundResource(R.drawable.carlarge);

            }
            header.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (news.seen.equals("")) {
                        news.seen = "1";
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        details.setLayoutParams(layoutParams);
                        details.setLayoutParams(layoutParams);
                        header.setBackgroundColor(Color.parseColor("#2d5c7a"));
                    } else {
                        news.seen = "";
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, 0);
                        details.setLayoutParams(layoutParams);
                        details.setLayoutParams(layoutParams);
                        if (position % 2 == 1) {
                            header.setBackgroundColor(Color.parseColor("#f4f4f4"));
                        } else {
                            header.setBackgroundColor(Color.parseColor("#fafafa"));
                        }
                    }
                }
            });
            if (position % 2 == 1) {
                header.setBackgroundColor(Color.parseColor("#f4f4f4"));
            } else {
                header.setBackgroundColor(Color.parseColor("#fafafa"));
            }
            holder = new ViewHolder();
            holder.title = title;
            holder.header = header;
            holder.details = details;
            holder.from = from;
            holder.to = to;
            holder.transport = trans;
            holder.transimage = transimage;
            v.setTag(holder);
        }

        return v;
    }

    public void setData(List<order> data) {
        clear();

        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                add(data.get(i));
            }
        }
    }
    public String getAddress(Context ctx, double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(ctx, Locale.ENGLISH);
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                String locality = address.getLocality();
                String city = address.getCountryName();
                String region_code = address.getCountryCode();
                String zipcode = address.getPostalCode();
                double lat = address.getLatitude();
                double lon = address.getLongitude();

                result.append(locality + " ");
                result.append(city + " " + region_code + " ");
                result.append(zipcode);

            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }
    public String getlocation(String from, String to) {
        String m ="";
        try {
            Geocoder geocoder;
            List<Address> adresses;
            geocoder = new Geocoder(context,
                    Locale.ENGLISH);
            adresses = geocoder.getFromLocation(Double.parseDouble(from),
                    Double.parseDouble(to), 1);
            m = (adresses.get(0).getAddressLine(0) + "  "
                    + adresses.get(0).getAddressLine(1) + "  "
                    + adresses.get(0).getAddressLine(2) );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return m;
    }
}
