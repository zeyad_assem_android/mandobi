package com.mandobiapp.driver;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class ClienthomeActivity extends Activity {
    LinearLayout order, profile, logout, neworder, fav;
    ProgressDialog progress;
    SharedPreferences storedata;
    private static String filename = "mlogin";
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clienthome);
        order = (LinearLayout) findViewById(R.id.order);
        profile = (LinearLayout) findViewById(R.id.profile);
        logout = (LinearLayout) findViewById(R.id.logout);
        neworder = (LinearLayout) findViewById(R.id.neworder);
        fav = (LinearLayout) findViewById(R.id.fav);
        neworder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ClienthomeActivity.this, NeworderActivity.class);
                startActivity(i);
                finish();
            }
        });
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ClienthomeActivity.this, ClientmyorderActivity.class);
                startActivity(i);

            }
        });
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ClienthomeActivity.this, ClientprofileActivity.class);
                startActivity(i);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager
                        .getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    new GetOrders().execute();
//                    try {
//                        String m = h("http://mandobiapp.com/c_m_s/logout_update.php", Gdata.id);
//                        JSONObject jo = new JSONObject(m);
//                        if (jo.getString("result").equals("true")) {
//                            storedata = getSharedPreferences(filename, 0);
//                            storedata.edit().clear().commit();
//                            Intent i = new Intent(ClienthomeActivity.this, ClientloginActivity.class);
//                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(i);
//                            finish();
////                            Gdata.backlist.removeAll(Gdata.backlist);
////                            Gdata.backlist.add("home");
//                        } else {
//                            Toast.makeText(ClienthomeActivity.this, jo.getString("data"), Toast.LENGTH_LONG).show();
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

                } else {
                    Toast t = Toast.makeText(ClienthomeActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }
            }
        });
        Intent i = new Intent("com.google.android.cdm.intent.REGISTER");
        i.putExtra("app", PendingIntent.getBroadcast(ClienthomeActivity.this,
                0, new Intent(), 0));
        i.putExtra("sender", "1003523032706");
        startService(i);

    }

    public String h(String url, String id) {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("member_id", new StringBody(id));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            Looper.prepare();
            progressDialog.dismiss();
            Toast t = Toast.makeText(ClienthomeActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            Looper.loop();
        }

        return json;
    }

    class GetOrders extends AsyncTask<Void, String, String> {
        String s;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(ClienthomeActivity.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = h("http://mandobiapp.com/c_m_s/logout_update.php", Gdata.id);
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        storedata = getSharedPreferences(filename, 0);
                        storedata.edit().clear().commit();
                        Intent i = new Intent(ClienthomeActivity.this, ClientloginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        finish();
//                            Gdata.backlist.removeAll(Gdata.backlist);
//                            Gdata.backlist.add("home");
                    } else {
                        Toast.makeText(ClienthomeActivity.this, j.getString("data"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }

    }

}


