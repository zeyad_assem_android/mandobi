package com.mandobiapp.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alexzh.circleimageview.CircleImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

//TODO deprecated
public class ProfileActivity extends Activity {
    TextView name, address, mobile, email, vcolor, dlno, save, edit, editphoto;
    CircleImageView image;
    LinearLayout small, medium, large;
    TextView switc;
    ProgressBar progress;
    AsyncHttpClient client = new AsyncHttpClient();
TextViewWithFont aval,unaval;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        aval = (TextViewWithFont) findViewById(R.id.aval);
        unaval = (TextViewWithFont) findViewById(R.id.unaval);
        name = (TextView) findViewById(R.id.name);
        address = (TextView) findViewById(R.id.address);
        mobile = (TextView) findViewById(R.id.mobile);
        email = (TextView) findViewById(R.id.email);
        vcolor = (TextView) findViewById(R.id.vcolor);
        dlno = (TextView) findViewById(R.id.dlno);
        edit = (TextView) findViewById(R.id.edit);
        editphoto = (TextView) findViewById(R.id.editp);
        small = (LinearLayout) findViewById(R.id.small);
        medium = (LinearLayout) findViewById(R.id.medium);
        large = (LinearLayout) findViewById(R.id.large);
        switc = (TextView) findViewById(R.id.Switch);
        image = (CircleImageView) findViewById(R.id.image);
        progress = (ProgressBar) findViewById(R.id.progress);

        name.setText(Gdata.name);
        address.setText(Gdata.address);
        mobile.setText(Gdata.phoneNumber);
        email.setText(Gdata.email);
        vcolor.setText(Gdata.vcolor);
        dlno.setText(Gdata.plateNumber);
        if (Gdata.taxitype.equals("1")) {
            small.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            small.setLayoutParams(layoutParams);
        } else if (Gdata.taxitype.equals("2")) {
            medium.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            medium.setLayoutParams(layoutParams);
        } else if (Gdata.taxitype.equals("3")) {
            large.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            large.setLayoutParams(layoutParams);
        }
        slidemandob.slidemenu(ProfileActivity.this);
        progress.setVisibility(View.VISIBLE);
        Picasso.with(ProfileActivity.this).load("http://mandobiapp.com/all_images/" + Gdata.photo).error(R.drawable.use).into(image, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                progress.setVisibility(View.GONE);

            }
        });
        editphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT < 19) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Complete action using"),
                            1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("image/jpeg");
                    startActivityForResult(intent, 1);
                }
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProfileActivity.this, Editprofile.class);
                startActivity(i);
                finish();
            }
        });
        Gdata.context = "mp";
        if (Gdata.isbusy.equals("0")) {
            switc.setBackgroundResource(R.drawable.on);
            aval.setEnabled(false);
            unaval.setEnabled(true);
        } else {
            switc.setBackgroundResource(R.drawable.off);
            unaval.setEnabled(false);
            aval.setEnabled(true);
        }
        switc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Gdata.isbusy.equals("0")) {
                    switc.setBackgroundResource(R.drawable.off);
                    Gdata.isbusy = "1";
                } else {
                    switc.setBackgroundResource(R.drawable.on);
                    Gdata.isbusy = "0";
                }
                client.get("http://mandobiapp.com/w_m_s/update_user_is_busy.php?member_id="+Gdata.id,new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(String s) {

                    }
                });
            }
        });
        aval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    switc.setBackgroundResource(R.drawable.on);
                    Gdata.isbusy = "0";
                aval.setEnabled(false);
                client.get("http://mandobiapp.com/w_m_s/update_user_is_busy.php?member_id="+Gdata.id,new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(String s) {

                    }
                });
            }
        });
        unaval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switc.setBackgroundResource(R.drawable.off);
                Gdata.isbusy = "1";
                unaval.setEnabled(false);
                client.get("http://mandobiapp.com/w_m_s/update_user_is_busy.php?member_id="+Gdata.id,new AsyncHttpResponseHandler(){
                    @Override
                    public void onSuccess(String s) {

                    }
                });
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("ResultLog", "reqCode : " + requestCode + "  resCode : " + resultCode);
        if (resultCode == Activity.RESULT_OK) {
            String realPath;
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(ProfileActivity.this, data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                realPath = RealPathUtil.getRealPathFromURI_API11to18(ProfileActivity.this, data.getData());

                // SDK > 19 (Android 4.4)
            else
                realPath = RealPathUtil.getRealPathFromURI_API19(ProfileActivity.this, data.getData());
            try {
                String m = h("http://mandobiapp.com/w_m_s/upload_mem_img.php", realPath, Gdata.id);
                JSONObject j = new JSONObject(m);
                if (j.getString("result").equals("true")) {
                    progress.setVisibility(View.VISIBLE);
                    editphoto.setVisibility(View.GONE);
                    JSONArray jary = j.getJSONArray("data");
                    JSONObject jo = jary.getJSONObject(0);
                    Gdata.photo = jo.getString("img_name");
                    Picasso.with(ProfileActivity.this).load("http://mandobiapp.com/all_images/" + jo.getString("img_name")).error(R.drawable.use).into(image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progress.setVisibility(View.GONE);
                            editphoto.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {
                            progress.setVisibility(View.GONE);
                            editphoto.setVisibility(View.VISIBLE);
                        }
                    });
                } else {

                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public static String h(String url, String path, String id) throws IOException {
        InputStream is = null;
        String json = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPostRequest = new HttpPost(url);
        MultipartEntity multipartEntity = new MultipartEntity();
        File file = new File(path);
        if (!path.equals("")) {
            multipartEntity.addPart("file", new FileBody(file));
        }
        multipartEntity.addPart("member_id", new StringBody(id));
        httpPostRequest.setEntity(multipartEntity);
        HttpResponse response = httpClient.execute(httpPostRequest);
        HttpEntity httpEntity = response.getEntity();
        is = httpEntity.getContent();
        int s = response.getStatusLine().getStatusCode();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return json;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
