package com.mandobiapp.driver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;


public class ClientloginActivity extends Activity {
    EditText username, password;
    TextViewWithFont reg, login;
    ProgressBar progress;
    SharedPreferences storedata;
    private static String filename = "mlogin";
    ProgressDialog progressDialog;
    TextView forget;
    String s = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientlogin);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        reg = (TextViewWithFont) findViewById(R.id.reg);
        login = (TextViewWithFont) findViewById(R.id.login);
        progress = (ProgressBar) findViewById(R.id.progress);
        forget = (TextView) findViewById(R.id.forget);
        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ClientloginActivity.this);

                //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle(getString(R.string.reset));

                // Setting Dialog Message
//                alertDialog.setMessage("Enter Password");
                final EditText input = new EditText(ClientloginActivity.this);
                input.setHint(getString(R.string.em));
                alertDialog.setView(input);
                if (Locale.getDefault().getLanguage().equals("ar")) {
                    input.setPadding(0, 0, 10, 0);
                    input.setGravity(Gravity.RIGHT | Gravity.CENTER);
                } else {
                    input.setPadding(10, 0, 0, 0);
                    input.setGravity(Gravity.LEFT | Gravity.CENTER);
                }
                alertDialog.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (input.getText().toString().equals("") && input.getText().toString().replaceAll(" ", "").equals("")) {
                                    Toast t = Toast.makeText(ClientloginActivity.this, getString(R.string.complete), Toast.LENGTH_LONG);
                                    t.setGravity(Gravity.CENTER, 0, 0);
                                    t.show();
                                } else {
                                    s = input.getText().toString();
                                    new GetOrder().execute();
                                }
                            }
                        });
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });
                alertDialog.show();
            }
        });
        if (Locale.getDefault().getLanguage().equals("en")) {
            username.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user, 0, 0, 0);
            password.setCompoundDrawablesWithIntrinsicBounds(R.drawable.password, 0, 0, 0);
        } else {
            username.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.user, 0);
            password.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.password, 0);
        }
        progress.setVisibility(View.INVISIBLE);
        slidepub.slidemenu(ClientloginActivity.this);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager
                        .getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    if (username.getText().toString().equals("") && username.getText().toString().replaceAll(" ", "").equals("") && password.getText().toString().equals("") && password.getText().toString().replaceAll(" ", "").equals("")) {
                        Toast t = Toast.makeText(ClientloginActivity.this, getString(R.string.complete), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else {
                        new GetOrders().execute();
                    }
                } else {
                    Toast t = Toast.makeText(ClientloginActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }
            }
        });
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ClientloginActivity.this, ClientregActivity.class);
                startActivity(i);
                Gdata.backlist.add("cl");
                finish();
            }
        });
        Gdata.context = "cl";

    }

    public String h(String url, String username, String password) {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("username", new StringBody(username));
            multipartEntity.addPart("password", new StringBody(password));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = null;
            response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            Looper.prepare();
            progressDialog.dismiss();
            Toast t = Toast.makeText(ClientloginActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            Looper.loop();
        }

        return json;
    }

    class GetOrders extends AsyncTask<Void, String, String> {
        String s;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(ClientloginActivity.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = h("http://mandobiapp.com/c_m_s/login_ws.php", username.getText().toString(), password.getText().toString());
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        storedata = getSharedPreferences(filename, 0);
                        SharedPreferences.Editor edit = storedata.edit();
                        edit.putString("id", jo.getString("member_id"));
                        edit.putString("type", "c");
                        edit.commit();
                        Gdata.user_id = jo.getString("member_id");
                        Gdata.user_name = jo.getString("f_name");
                        Gdata.user_username = jo.getString("user_name");
                        Gdata.user_phone = jo.getString("mobile");
                        Gdata.user_photo = jo.getString("member_image");
                        Gdata.user_email = jo.getString("email");
                        Gdata.status_type = "c";
                        Intent i = new Intent(ClientloginActivity.this, NeworderActivity.class);
                        startActivity(i);
                        finish();
                        Gdata.backlist.removeAll(Gdata.backlist);
                    } else {
                        Toast t = Toast.makeText(ClientloginActivity.this, getString(R.string.sure), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                        progressDialog.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }

    }

    class GetOrder extends AsyncTask<Void, String, String> {
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(ClientloginActivity.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = hh("http://mandobiapp.com/c_m_s/reset_password.php?lang=0", s);
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        Toast t = Toast.makeText(ClientloginActivity.this, getString(R.string.c), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else {
                        Toast t = Toast.makeText(ClientloginActivity.this, j.getString("data"), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }

    }


    public String hh(String url, String email) {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("email", new StringBody(email));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            Log.e("email", "Email " + email);
            Looper.prepare();
            Toast t = Toast.makeText(ClientloginActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            e.printStackTrace();
            Looper.loop();
        }

        return json;
    }

    @Override
    public void onBackPressed() {
        Gdata.back(ClientloginActivity.this);

    }
}
