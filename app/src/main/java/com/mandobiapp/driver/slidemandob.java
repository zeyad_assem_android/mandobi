package com.mandobiapp.driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alexzh.circleimageview.CircleImageView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by elkholy on 23/12/2015.
 */
public class slidemandob {
    public static SlidingMenu mLMenu;
    public static SharedPreferences storedata;
    private static String filename = "mlogin";
    public final static ArrayList<String> backlist = new ArrayList<>();
    public static ProgressDialog progressDialog;
    public static Context con;
    public static GoogleMap mmap;
    public static double s;

    public static void slidemenu(final Context context) {
        storedata = context.getSharedPreferences(filename, 0);
        mLMenu = new SlidingMenu(context);
        if (Locale.getDefault().getLanguage().equals("ar")) {
            mLMenu.setMode(SlidingMenu.RIGHT);
        } else {
            mLMenu.setMode(SlidingMenu.LEFT);
        }
        mLMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        mLMenu.setShadowWidthRes(R.dimen.shadow_width);
        mLMenu.setShadowDrawable(R.drawable.shadow);
        mLMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        mLMenu.setFadeDegree(0.45f);
        mLMenu.attachToActivity(((Activity) context)
                , SlidingMenu.SLIDING_CONTENT);
        mLMenu.setMenu(R.layout.menumandob);
        final LinearLayout lin = (LinearLayout) mLMenu.findViewById(R.id.lin);
        final LinearLayout lin1 = (LinearLayout) mLMenu.findViewById(R.id.lin1);
        final LinearLayout lin2 = (LinearLayout) mLMenu.findViewById(R.id.lin2);
        final LinearLayout lin3 = (LinearLayout) mLMenu.findViewById(R.id.lin3);
        final LinearLayout lin4 = (LinearLayout) mLMenu.findViewById(R.id.lin4);
        final TextView menu = (TextView) ((Activity) context).findViewById(R.id.menu);
        final CircleImageView image = (CircleImageView) mLMenu.findViewById(R.id.image);
        final TextViewWithFont name = (TextViewWithFont) mLMenu.findViewById(R.id.name);
        final ProgressBar progress = (ProgressBar) mLMenu.findViewById(R.id.progress);
//        if (mmap == null) {
//            mmap = ((MapFragment) ((Activity)context).getFragmentManager().findFragmentById(
//                    R.id.map)).getMap();
//        }
//        mmap.setMyLocationEnabled(true);
//        mmap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        try {
//            final GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
//                @Override
//                public void onMyLocationChange(Location location) {
//                        Gdata.lat = Double.toString(location.getLatitude());
//                        Gdata.longt = Double.toString(location.getLongitude());
//                        Gdata.count++;
//                     updateuserplace(context ,"http://mandobiapp.com/w_m_s/update_user_place2.php", Gdata.longt, Double.toString(location.getLatitude()));
//
//                }
//            };
//            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo activeNetworkInfo = connectivityManager
//                    .getActiveNetworkInfo();
//            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
//                final Handler mHandler = new Handler();
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        // TODO Auto-generated method stub
//                        while (true) {
//                            try {
//                                Thread.sleep(10000);
//                                mHandler.post(new Runnable() {
//
//                                    @Override
//                                    public void run() {
//                                        mmap.setOnMyLocationChangeListener(myLocationChangeListener);
//                                        // TODO Auto-generated method stub
//                                        // Write your code here to update the UI.
//                                    }
//                                });
//                            } catch (Exception e) {
//                                // TODO: handle exception
//                            }
//                        }
//                    }
//                }).start();
//            } else {
//               // dialog.dialog(context, getString(R.string.check));
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        try {
            if (mmap == null) {
                //                Hashed by zeyad
//                mmap = ((MapFragment) ((Activity) context).getFragmentManager().findFragmentById(
//                        R.id.map)).getMap();
            }
            mmap.setMyLocationEnabled(true);
            mmap.getUiSettings().setZoomControlsEnabled(true);
            GoogleMap.OnMyLocationChangeListener myLocationChangeListener = new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                    if (Gdata.count == 0) {
                        Gdata.lat = Double.toString(location.getLatitude());
                        Gdata.longt = Double.toString(location.getLongitude());
                        Gdata.count++;
                        updateuserplace(context, "http://mandobiapp.com/w_m_s/update_user_place2.php", Gdata.longt, Double.toString(location.getLatitude()));
                    } else {
                        s = Gdata.distance(location.getLatitude(), location.getLongitude(), Double.parseDouble(Gdata.lat), Double.parseDouble(Gdata.longt));
                        if (s >= 20) {
                            Gdata.lat = Double.toString(location.getLatitude());
                            Gdata.longt = Double.toString(location.getLongitude());
                            Gdata.count++;
                            updateuserplace(context, "http://mandobiapp.com/w_m_s/update_user_place2.php", Gdata.longt, Double.toString(location.getLatitude()));
                        }
                    }

                }
            };
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//                if (statusOfGPS) {
                mmap.setOnMyLocationChangeListener(myLocationChangeListener);
//                }else {
//                    Looper.prepare();
//                    Toast t = Toast.makeText(context, context.getString(R.string.check), Toast.LENGTH_LONG);
//                    t.setGravity(Gravity.CENTER, 0, 0);
//                    t.show();
//                    Looper.loop();
//                }
            } else {
                Looper.prepare();
                Toast t = Toast.makeText(context, context.getString(R.string.check), Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                Looper.loop();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        if (mmap == null) {
//            mmap = ((MapFragment) getFragmentManager().findFragmentById(
//                    R.id.map)).getMap();
//        }
//        mmap.setMyLocationEnabled(true);
//        mmap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        GoogleMap.OnMyLocationChangeListener = new OnMyLocationChangeListener(){
//
//            @Override
//            public void onMyLocationChange(Location location) {
//
//            }
//        });
        progress.setVisibility(View.VISIBLE);
        Picasso.with(context).load("http://mandobiapp.com/all_images/" + Gdata.photo).error(R.drawable.use).into(image, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                progress.setVisibility(View.GONE);

            }
        });
        name.setText(Gdata.name);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLMenu.showMenu(true);
            }
        });
        lin4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!Gdata.context.equals("cp")) {
                Gdata.backlist.add(Gdata.context);
//                }
                Intent i = new Intent(context, ChangeActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();

            }
        });
        lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("mp")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, ProfileActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();

            }
        });
        lin1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("mr")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, OrderActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();
            }
        });
        lin3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Gdata.context.equals("mmc")) {
                    Gdata.backlist.add(Gdata.context);
                }
                Intent i = new Intent(context, ProfitsActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                mLMenu.toggle();
            }
        });
        lin2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new GetOrders().execute();
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
                String response = null;
                response = h("http://mandobiapp.com/w_m_s/logout_update.php", Gdata.id);
                if (response.contains("{\"result\":")) {
                    JSONObject j = null;
                    try {
                        j = new JSONObject(response);
                        if (j.getString("result").equals("true")) {
                            storedata.edit().clear().commit();
                            Intent i = new Intent(context, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(i);
                            ((Activity) context).finish();//
                            Gdata.backlist.removeAll(Gdata.backlist);
                            Gdata.count = 0;
                            System.runFinalizersOnExit(true);
                            System.exit(0);
                            android.os.Process.killProcess(android.os.Process.myPid());
//                            Gdata.backlist.add("home");
                        } else {
                            Toast.makeText(con, j.getString("data"), Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // progressDialog.dismiss();
                }
                mLMenu.toggle();
            }
        });


    }

    public static class GetOrders extends AsyncTask<Void, String, String> {
        String s;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            //progressDialog = ProgressDialog.show(con, con.getString(R.string.wait), con.getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = h("http://mandobiapp.com/w_m_s/logout_update.php", Gdata.id);
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        storedata.edit().clear().commit();
                        Intent i = new Intent(con, LoginActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        con.startActivity(i);
                        ((Activity) con).finish();//                            Gdata.backlist.removeAll(Gdata.backlist);
//                            Gdata.backlist.add("home");
                    } else {
                        Toast.makeText(con, j.getString("data"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // progressDialog.dismiss();
            }
        }

    }

    public static String h(String url, String id) {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("member_id", new StringBody(id));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            Looper.prepare();
            // progressDialog.dismiss();
            Toast t = Toast.makeText(con, con.getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            Looper.loop();
        }

        return json;
    }

    public static String excutePost(String targetURL, String urlParameters) {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" +
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {
            Looper.prepare();
            return null;
        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
    }


    public static void updateuserplace(final Context context, String url, String lon, String lat) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String response = null;
        try {
            response = excutePost(url, "member_id=" + URLEncoder.encode(Gdata.id, "UTF-8")
                    + "&longitude=" + URLEncoder.encode(lon, "UTF-8") + "&latitude=" + URLEncoder.encode(lat, "UTF-8"));
            if (response != null) {
                JSONObject j = new JSONObject(response);
                if (j.getString("result").equals("true")) {

                } else {

                }
            } else {
                Looper.prepare();
                Toast t = Toast.makeText(context, context.getString(R.string.check), Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                Looper.loop();
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
