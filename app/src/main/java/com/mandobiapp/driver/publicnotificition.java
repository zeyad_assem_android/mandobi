package com.mandobiapp.driver;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class publicnotificition extends Activity {
    TextViewWithFont title, content, t;
    TextView menu;
    SharedPreferences storedata;
    private static String filename = "mlogin";
    String id, type;
    AsyncHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicnotificition);
        storedata = getSharedPreferences(filename, 0);
        id = storedata.getString("id", "vgcvc");
        client = new AsyncHttpClient();
        content = (TextViewWithFont) findViewById(R.id.content);
        t = (TextViewWithFont) findViewById(R.id.t);
        t.setText(Gdata.t);
        content.setText(Gdata.body);
        storedata = getSharedPreferences(filename, 0);
        Gdata.id = storedata.getString("id", "vgcvc");
        client.get("http://mandobiapp.com/c_m_s/get_user_byid.php?u_id=" + id, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                JSONObject j = null;
                try {
                    j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        Gdata.user_id = id;
                        Gdata.user_name = jo.getString("f_name");
                        Gdata.user_username = jo.getString("user_name");
                        Gdata.user_phone = jo.getString("mobile");
                        Gdata.user_photo = jo.getString("member_image");
                        Gdata.user_email = jo.getString("email");
                        Gdata.status_type = "c";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
