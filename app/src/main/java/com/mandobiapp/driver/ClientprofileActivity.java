package com.mandobiapp.driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alexzh.circleimageview.CircleImageView;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class ClientprofileActivity extends Activity {
    TextView name, mobile, email, save, edit, editphoto;
    CircleImageView image;
    SwitchCompat switc;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientprofile);
        name = (TextView) findViewById(R.id.name);
        mobile = (TextView) findViewById(R.id.mobile);
        email = (TextView) findViewById(R.id.email);
        edit = (TextView) findViewById(R.id.edit);
        editphoto = (TextView) findViewById(R.id.editp);
        image = (CircleImageView) findViewById(R.id.image);
        progress = (ProgressBar) findViewById(R.id.progress);
        name.setText(Gdata.user_name);
        mobile.setText(Gdata.user_phone);
        email.setText(Gdata.user_email);
        progress.setVisibility(View.VISIBLE);
        slideclient.slidemenu(ClientprofileActivity.this);
        Gdata.context="cp";
        Picasso.with(ClientprofileActivity.this).load("http://mandobiapp.com/all_images/" + Gdata.user_photo).error(R.drawable.use).into(image, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                progress.setVisibility(View.GONE);

            }
        });
        editphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT < 19) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Complete action using"),
                            1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("image/jpeg");
                    startActivityForResult(intent, 1);
                }
            }
        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ClientprofileActivity.this, ClienteditActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("ResultLog", "reqCode : " + requestCode + "  resCode : " + resultCode);
        if (resultCode == Activity.RESULT_OK) {
            String realPath;
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(ClientprofileActivity.this, data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                realPath = RealPathUtil.getRealPathFromURI_API11to18(ClientprofileActivity.this, data.getData());

                // SDK > 19 (Android 4.4)
            else
                realPath = RealPathUtil.getRealPathFromURI_API19(ClientprofileActivity.this, data.getData());
            try {
                String m = h("http://mandobiapp.com/c_m_s/upload_mem_img.php", realPath, Gdata.user_id);
                JSONObject j = new JSONObject(m);
                if (j.getString("result").equals("true")) {
                    progress.setVisibility(View.VISIBLE);
                    editphoto.setVisibility(View.GONE);
                    JSONArray jary = j.getJSONArray("data");
                    JSONObject jo = jary.getJSONObject(0);
                    Gdata.user_photo = jo.getString("img_name");
                    Picasso.with(ClientprofileActivity.this).load("http://mandobiapp.com/all_images/" + jo.getString("img_name")).error(R.drawable.use).into(image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            progress.setVisibility(View.GONE);
                            editphoto.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onError() {
                            progress.setVisibility(View.GONE);
                            editphoto.setVisibility(View.VISIBLE);
                        }
                    });
                } else {

                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static String h(String url, String path, String id) throws IOException {
        InputStream is = null;
        String json = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPostRequest = new HttpPost(url);
        MultipartEntity multipartEntity = new MultipartEntity();
        File file = new File(path);
        if (!path.equals("")) {
            multipartEntity.addPart("file", new FileBody(file));
        }
        multipartEntity.addPart("member_id", new StringBody(id));
        httpPostRequest.setEntity(multipartEntity);
        HttpResponse response = httpClient.execute(httpPostRequest);
        HttpEntity httpEntity = response.getEntity();
        is = httpEntity.getContent();
        int s = response.getStatusLine().getStatusCode();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return json;
    }
//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_HOME);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//        finish();
//    }
@Override
public void onBackPressed() {
    Gdata.back(ClientprofileActivity.this);
}
}
