package com.mandobiapp.driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class ClientregActivity extends Activity {
    TextView image,  reg;
    EditText name, username, password, cpassword, mobile, email;
    String file="";
    ProgressBar progress;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientreg);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        image = (TextView) findViewById(R.id.image);
        reg = (TextView) findViewById(R.id.reg);
        name = (EditText) findViewById(R.id.name);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        cpassword = (EditText) findViewById(R.id.cpassword);
        mobile = (EditText) findViewById(R.id.mobile);
        email = (EditText) findViewById(R.id.email);
        progress = (ProgressBar)findViewById(R.id.progress);
        slidepub.slidemenu(ClientregActivity.this);
        Gdata.context="cr";
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager
                        .getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    if (username.getText().toString().equals("") || username.getText().toString().replaceAll(" ", "").equals("")  || password.getText().toString().equals("") || password.getText().toString().replaceAll(" ", "").equals("") || name.getText().toString().equals("") || name.getText().toString().replaceAll(" ", "").equals("") || mobile.getText().toString().equals("") || mobile.getText().toString().replaceAll(" ", "").equals("") ) {
                        Toast t = Toast.makeText(ClientregActivity.this, getString(R.string.complete), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else if (password.getText().toString().length() < 7) {
                        Toast t = Toast.makeText(ClientregActivity.this, getString(R.string.passwordu), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else if (!password.getText().toString().equals(cpassword.getText().toString()) && password.getText().toString().length() > 6) {
                        Toast t = Toast.makeText(ClientregActivity.this, getString(R.string.pnot), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else if (Gdata.emailValidator(email.getText().toString()) == false || email.getText().toString().equals("") || email.getText().toString().replaceAll(" ", "").equals("")) {
                        Toast t = Toast.makeText(ClientregActivity.this, getString(R.string.emailc), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else {
                        new GetOrders().execute();
//                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
//                                .permitAll().build();
//                        StrictMode.setThreadPolicy(policy);
//                        try {
//                            progress.setVisibility(View.VISIBLE);
//                            String m = h("http://mandobiapp.com/c_m_s/register_ws.php", file, name.getText().toString(), username.getText().toString(), password.getText().toString(), email.getText().toString(), mobile.getText().toString());
//                            JSONObject j = new JSONObject(m);
//                            if (j.getString("result").equals("true")) {
//                                progress.setVisibility(View.INVISIBLE);
//                                Intent i = new Intent(ClientregActivity.this, ClientloginActivity.class);
//                                startActivity(i);
//                                finish();
//                            } else {
//                                progress.setVisibility(View.INVISIBLE);
//                                Toast t = Toast.makeText(ClientregActivity.this, j.getString("data"), Toast.LENGTH_LONG);
//                                t.setGravity(Gravity.CENTER, 0, 0);
//                                t.show();
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }

                }
            }


        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT < 19) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Complete action using"),
                            1);
                } else {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("image/jpeg");
                    startActivityForResult(intent, 1);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i("ResultLog", "reqCode : " + requestCode + "  resCode : " + resultCode);
        if (resultCode == Activity.RESULT_OK) {
            String realPath;
            // SDK < API11
            if (Build.VERSION.SDK_INT < 11)
                file = RealPathUtil.getRealPathFromURI_BelowAPI11(ClientregActivity.this, data.getData());

                // SDK >= 11 && SDK < 19
            else if (Build.VERSION.SDK_INT < 19)
                file = RealPathUtil.getRealPathFromURI_API11to18(ClientregActivity.this, data.getData());

                // SDK > 19 (Android 4.4)
            else
                file = RealPathUtil.getRealPathFromURI_API19(ClientregActivity.this, data.getData());

            Log.i("UploadReturn", "path : " + file);
        }
    }






    public  String h(String url, String path, String f_name, String username, String password, String email, String phon)  {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            File file = new File(path);
            if (!path.equals("")) {
                multipartEntity.addPart("file", new FileBody(file));
            }
            multipartEntity.addPart("f_name", new StringBody(f_name));
            multipartEntity.addPart("username", new StringBody(username));
            multipartEntity.addPart("password", new StringBody(password));
            multipartEntity.addPart("email", new StringBody(email));
            multipartEntity.addPart("mobile", new StringBody(phon));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "utf-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();
            } catch (Exception e) {
                Looper.prepare();
                progressDialog.dismiss();
                Toast t = Toast.makeText(ClientregActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                Looper.loop();
            }

        return json;
    }
    class GetOrders extends AsyncTask<Void, String, String> {
        String s;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(ClientregActivity.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = h("http://mandobiapp.com/c_m_s/register_ws.php", file, name.getText().toString(), username.getText().toString(), password.getText().toString(), email.getText().toString(), mobile.getText().toString());
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        progress.setVisibility(View.INVISIBLE);
                        Intent i = new Intent(ClientregActivity.this, ClientloginActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        progress.setVisibility(View.INVISIBLE);
                        Toast t = Toast.makeText(ClientregActivity.this, j.getString("data"), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }

    }
    @Override
    public void onBackPressed() {
        Gdata.back(ClientregActivity.this);
    }
}
