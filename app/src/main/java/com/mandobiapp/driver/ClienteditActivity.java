package com.mandobiapp.driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class ClienteditActivity extends Activity {
    TextView reg;
    EditText name, username, password, cpassword, mobile, email;
    String file;
    ProgressBar progress;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientedit);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Gdata.context="ce";
        reg = (TextView) findViewById(R.id.reg);
        name = (EditText) findViewById(R.id.name);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        cpassword = (EditText) findViewById(R.id.cpassword);
        mobile = (EditText) findViewById(R.id.mobile);
        email = (EditText) findViewById(R.id.email);
        progress = (ProgressBar)findViewById(R.id.progress);
        Toast t = Toast.makeText(ClienteditActivity.this, getString(R.string.passwordletter), Toast.LENGTH_LONG);
        t.setGravity(Gravity.CENTER, 0, 0);
        t.show();
        slideclient.slidemenu(ClienteditActivity.this);
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager
                        .getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    if (username.getText().toString().equals("") || username.getText().toString().replaceAll(" ", "").equals("") || name.getText().toString().equals("") || name.getText().toString().replaceAll(" ", "").equals("") || mobile.getText().toString().equals("") || mobile.getText().toString().replaceAll(" ", "").equals("") ) {
                        Toast t = Toast.makeText(ClienteditActivity.this, getString(R.string.complete), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else if (password.getText().toString().length() < 7 && password.getText().toString().length() != 0) {
                        Toast t = Toast.makeText(ClienteditActivity.this, getString(R.string.passwordu), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else if (!password.getText().toString().equals(cpassword.getText().toString()) && password.getText().toString().length() > 6) {
                        Toast t = Toast.makeText(ClienteditActivity.this, getString(R.string.pnot), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else if (Gdata.emailValidator(email.getText().toString()) == false || email.getText().toString().equals("") || email.getText().toString().replaceAll(" ", "").equals("")) {
                        Toast t = Toast.makeText(ClienteditActivity.this, getString(R.string.emailc), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else {
                        new GetOrders().execute();
//                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
//                                .permitAll().build();
//                        StrictMode.setThreadPolicy(policy);
//                        try {
//                            progress.setVisibility(View.VISIBLE);
//                            String m = h("http://mandobiapp.com/w_m_s/update_profile.php", Gdata.id, name.getText().toString(), username.getText().toString(), password.getText().toString(), email.getText().toString(), mobile.getText().toString(), address.getText().toString(), vcolor.getText().toString(), dln.getText().toString(), type);
//                            JSONObject j = new JSONObject(m);
//                            if (j.getString("result").equals("true")) {
//                                progress.setVisibility(View.INVISIBLE);
//                                Intent i = new Intent(Editprofile.this, ProfileActivity.class);
//                                Gdata.name=name.getText().toString();
//                                Gdata.username=username.getText().toString();
//                                Gdata.phoneNumber=mobile.getText().toString();
//                                Gdata.address=address.getText().toString();
//                                Gdata.email=email.getText().toString();
//                                Gdata.vcolor=vcolor.getText().toString();
//                                Gdata.taxitype=type;
//                                Gdata.plateNumber=dln.getText().toString();
//                                startActivity(i);
//                                finish();
//                            } else {
//                                progress.setVisibility(View.INVISIBLE);
//                                Toast t = Toast.makeText(Editprofile.this, j.getString("data"), Toast.LENGTH_LONG);
//                                t.setGravity(Gravity.CENTER, 0, 0);
//                                t.show();
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
                    }

                }
            }


        });
        name.setText(Gdata.user_name);
        username.setText(Gdata.user_username);
        mobile.setText(Gdata.user_phone);
        email.setText(Gdata.user_email);
    }
    public  String h(String url,String id, String f_name, String username, String password, String email, String phon)  {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("f_name", new StringBody(f_name));
            multipartEntity.addPart("member_id",new StringBody(id));
            multipartEntity.addPart("username", new StringBody(username));
            multipartEntity.addPart("password", new StringBody(password));
            multipartEntity.addPart("email", new StringBody(email));
            multipartEntity.addPart("mobile", new StringBody(phon));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            Looper.prepare();
            progressDialog.dismiss();
            Toast t = Toast.makeText(ClienteditActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            Looper.loop();
        }

        return json;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ClienteditActivity.this,ClientprofileActivity.class);
        startActivity(i);
        finish();
    }

    class GetOrders extends AsyncTask<Void, String, String> {
        String s;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(ClienteditActivity.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = h("http://mandobiapp.com/c_m_s/update_profile.php", Gdata.user_id, name.getText().toString(), username.getText().toString(), password.getText().toString(), email.getText().toString(), mobile.getText().toString());
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        progress.setVisibility(View.INVISIBLE);
                        Intent i = new Intent(ClienteditActivity.this, ClientprofileActivity.class);
                        Gdata.user_name=name.getText().toString();
                        Gdata.user_username=username.getText().toString();
                        Gdata.user_phone=mobile.getText().toString();
                        Gdata.user_email=email.getText().toString();
                        startActivity(i);
                        finish();
                    } else {
                        Looper.prepare();
                        Toast t = Toast.makeText(ClienteditActivity.this, j.getString("data"), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                        Looper.loop();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }

    }

}
