package com.mandobiapp.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class FollowActivity extends Activity {
    GoogleMap mmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow);
        try {
            if (mmap == null) {
                //                Hashed by zeyad
//                mmap = ((MapFragment) getFragmentManager().findFragmentById(
//                        R.id.map)).getMap();
            }
            mmap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mmap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon))).title(
                    "FROM LOCATION").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
            mmap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon))).title(
                    "TO LOCATION").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
            mmap.getUiSettings().setMyLocationButtonEnabled(true);
            mmap.getUiSettings().setZoomGesturesEnabled(true);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon))).zoom(12).build();
            mmap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
//        mmap.addPolyline(new PolylineOptions()
//                .add(new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon)), new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon)))
//                .width(10)
//                .color(Color.parseColor("#f9b21a")).geodesic(true));
        } catch (Exception e) {
            e.printStackTrace();
        }
        LatLng origin = new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon));
        LatLng dest = new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon));
        String url = getDirectionsUrl(origin, dest);
        DownloadTask downloadTask = new DownloadTask();
        downloadTask.execute(url);

    }
    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + ","
                + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + parameters;
        return url;
    }

    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                parser parser = new parser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            Log.e("results", result + "");
            if (result.size() < 1) {
                Toast.makeText(FollowActivity.this, "No Points", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);
                Log.e("points", path + "");
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    if (j == 0) {

                    } else if (j == 1) {
                    } else if (j > 1) {
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                }
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.parseColor("#f9b21a"));
            }
            mmap.addPolyline(lineOptions);
        }
    }
}
