package com.mandobiapp.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by elkholy on 21/01/2016.
 */
public class mandobfollow extends Activity {
    GoogleMap mmap;
    AsyncHttpClient client = new AsyncHttpClient();
    ProgressDialog progressDialog;
    private static String filename = "mlogin";
    SharedPreferences storedata;
    String id, type;
    int r;
    Marker mark;
    TextViewWithFont done;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow);
        done = (TextViewWithFont) findViewById(R.id.done);
        storedata = getSharedPreferences(filename, 0);
        id = storedata.getString("id", "vgcvc");
        slideclient.slidemenu(mandobfollow.this);
        client.get("http://mandobiapp.com/c_m_s/get_user_byid.php?u_id=" + id, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                JSONObject j = null;
                try {
                    j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        Gdata.user_id = id;
                        Gdata.user_name = jo.getString("f_name");
                        Gdata.user_username = jo.getString("user_name");
                        Gdata.user_phone = jo.getString("mobile");
                        Gdata.user_photo = jo.getString("member_image");
                        Gdata.user_email = jo.getString("email");
                        Gdata.status_type = "c";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        progressDialog = ProgressDialog.show(mandobfollow.this, getString(R.string.wait), getString(R.string.load), true);
        client.get(String.format("http://mandobiapp.com/c_m_s/view_msg_by_id.php?msg_id=%s&type=recieve", Gdata.message_id), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                JSONObject j = null;
                try {
                    j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        Gdata.from_lat = jo.getString("from_latitude");
                        Gdata.from_lon = jo.getString("from_longitude");
                        Gdata.to_lat = jo.getString("to_latitude");
                        Gdata.to_lon = jo.getString("to_longitude");
                        Gdata.type = jo.getString("vehicle_type");
                        client.get("http://mandobiapp.com/w_m_s/get_user_byid.php?u_id=" + Gdata.id, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(String s) {
                                JSONObject j = null;
                                try {
                                    j = new JSONObject(s);
                                    if (j.getString("result").equals("true")) {
                                        JSONArray jary = j.getJSONArray("data");
                                        JSONObject jo = jary.getJSONObject(0);
                                        Gdata.name = jo.getString("f_name");
                                        Gdata.longt = jo.getString("longitude");
                                        Gdata.lat = jo.getString("latitude");
                                        Gdata.taxitype = jo.getString("taxi_type");
                                        if (Gdata.taxitype.equals("1")) {
                                            r = R.drawable.smal_car;
                                        } else if (Gdata.taxitype.equals("2")) {
                                            r = R.drawable.med_car;
                                        } else if (Gdata.taxitype.equals("3")) {
                                            r = R.drawable.large_car;
                                        }
                                        try {
                                            if (mmap == null) {
                                                //                Hashed by zeyad
//                                                mmap = ((MapFragment) getFragmentManager().findFragmentById(
//                                                        R.id.map)).getMap();
                                            }
                                            mmap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                                            mmap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon))).title(
                                                    "FROM LOCATION").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
                                            mmap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon))).title(
                                                    "TO LOCATION").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
                                            mark = mmap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.lat), Double.parseDouble(Gdata.longt))).title(
                                                    "MANDOB LOCATION").icon(BitmapDescriptorFactory.fromResource(r)));
                                            mmap.getUiSettings().setMyLocationButtonEnabled(true);
                                            mmap.getUiSettings().setZoomGesturesEnabled(true);
                                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                                    .target(new LatLng(Double.parseDouble(Gdata.lat), Double.parseDouble(Gdata.longt))).zoom(12).build();
                                            mmap.animateCamera(CameraUpdateFactory
                                                    .newCameraPosition(cameraPosition));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        LatLng origin = new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon));
                                        LatLng dest = new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon));
                                        String url = getDirectionsUrl(origin, dest);
                                        DownloadTask downloadTask = new DownloadTask();
                                        downloadTask.execute(url);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        });


        final Handler mHandler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (true) {
                    try {
                        Thread.sleep(10000);
                        mHandler.post(new Runnable() {

                            @Override
                            public void run() {
                                client.get("http://mandobiapp.com/w_m_s/get_user_byid.php?u_id=" + Gdata.id, new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(String s) {
                                        JSONObject j = null;
                                        try {
                                            j = new JSONObject(s);
                                            if (j.getString("result").equals("true")) {
                                                JSONArray jary = j.getJSONArray("data");
                                                JSONObject jo = jary.getJSONObject(0);
                                                Gdata.longt = jo.getString("longitude");
                                                Gdata.lat = jo.getString("latitude");
                                                Gdata.taxitype = jo.getString("taxi_type");
                                                mark.remove();
                                                mark = mmap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.lat), Double.parseDouble(Gdata.longt))).title(
                                                        "Location of mandob").icon(BitmapDescriptorFactory.fromResource(r)));
                                                mmap.getUiSettings().setMyLocationButtonEnabled(true);
                                                mmap.getUiSettings().setZoomGesturesEnabled(true);
                                                CameraPosition cameraPosition = new CameraPosition.Builder()
                                                        .target(new LatLng(Double.parseDouble(Gdata.lat), Double.parseDouble(Gdata.longt))).zoom(12).build();
                                                mmap.animateCamera(CameraUpdateFactory
                                                        .newCameraPosition(cameraPosition));

                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                            }
                        });
                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            }
        }).start();
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                client.get(String.format("http://mandobiapp.com/c_m_s/finish_process.php?msg_id=%s&lang=1", Gdata.message_id), new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String s) {
                        try {
                            JSONObject j = new JSONObject(s);
                            if (j.getString("result").equals("success")) {
                                final AlertDialog.Builder builder = new AlertDialog.Builder(mandobfollow.this);
                                builder.setTitle(getString(R.string.success));
                                builder.setMessage(j.getString("data"));
                                builder.show();
                                new Thread() {
                                    @Override
                                    public void run() {
                                        try {
                                            sleep(2500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        } finally {
                                            Intent i = new Intent(mandobfollow.this, NeworderActivity.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    }
                                }.start();
                            } else {
                                Toast t = Toast.makeText(mandobfollow.this, j.getString("data"), Toast.LENGTH_LONG);
                                t.setGravity(Gravity.CENTER, 0, 0);
                                t.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + ","
                + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + parameters;
        return url;
    }

    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                parser parser = new parser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            Log.e("results", result + "");
            if (result.size() < 1) {
                Toast.makeText(mandobfollow.this, "No Points", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);
                Log.e("points", path + "");
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    if (j == 0) {

                    } else if (j == 1) {
                    } else if (j > 1) {
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                }
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.parseColor("#f9b21a"));
            }
            mmap.addPolyline(lineOptions);
        }
    }

    @Override
    public void onBackPressed() {
        Gdata.back(mandobfollow.this);
    }
}
