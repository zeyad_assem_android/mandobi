package com.mandobiapp.driver.engine.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Zeyad Assem on 02/09/16.
 */
public class SharedPreferenceManager {
    private static SharedPreferenceManager sharedPreferenceManager = null;
    private static SharedPreferences sharedPreference;
    private static final String sharedPreferenceFileName = "mlogin";
    private static SharedPreferences.Editor editor;
    private SharedPreferenceManager(){
    }
    //TODO a singleton pattern to apply on a shared preference manager.
    public static SharedPreferenceManager getInstance(Context context){
        sharedPreference = context.getSharedPreferences(sharedPreferenceFileName,Context.MODE_PRIVATE);
        editor = sharedPreference.edit();
        if(sharedPreferenceManager == null){
            return new SharedPreferenceManager();
        }else
            return sharedPreferenceManager;
    }

    public static void clearAllSharedPreferenceData(){
        editor.clear();
        editor.commit();
    }

    public static void changeLanguage(String languageToLoad){
        editor.putString("languageToLoad",languageToLoad );
        editor.commit();
    }

    public String getCurrentLanguage(){
        return sharedPreference.getString("languageToLoad","default");
    }
}
