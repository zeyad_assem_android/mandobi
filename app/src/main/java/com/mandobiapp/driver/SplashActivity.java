package com.mandobiapp.driver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.ui.activities.driver.main.DriverMainActivity;
import com.mandobiapp.driver.utilities.Logger;


public class SplashActivity extends Activity {
    SharedPreferences storedata;
    private static String filename = "mlogin";
    String id,type;
    AsyncHttpClient client = new AsyncHttpClient();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        storedata = getSharedPreferences(filename, 0);
        id = storedata.getString("id", "vgcvc");
        type = storedata.getString("type", "vgcvc");
        Thread th = new Thread(){
            @Override
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    if (id.equals("vgcvc")){
                        Intent i = new Intent(SplashActivity.this,LoginActivity.class);
                        startActivity(i);
                        finish();
                    }else {
                        if (type.equals("m")){
                            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo activeNetworkInfo = connectivityManager
                                    .getActiveNetworkInfo();
                            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                                client.get(URLConstants.BASE_URL+"w_m_s/get_user_byid.php?u_id=" + id, new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onFailure(Throwable throwable, String s) {
                                        Looper.prepare();
                                        Toast t = Toast.makeText(SplashActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                                        t.setGravity(Gravity.CENTER, 0, 0);
                                        t.show();
                                        Looper.loop();
                                    }
                                    @Override
                                    public void onSuccess(String s) {
                                        Logger.LOG_I("Response - m",s);
                                     //   if (s.contains("{\"result\":")) {
                                            JSONObject j = null;
                                            try {
                                                j = new JSONObject(s);
                                                if (j.getString("result").equals("true")) {
                                                    JSONArray jary = j.getJSONArray("data");
                                                    JSONObject jo = jary.getJSONObject(0);
                                                    Gdata.id = id;
                                                    Gdata.name = jo.getString("f_name");
                                                    Gdata.username = jo.getString("user_name");
                                                    Gdata.isbusy = jo.getString("is_busy");
                                                    Gdata.phoneNumber = jo.getString("mobile");
                                                    Gdata.taxitype = jo.getString("taxi_type");
                                                    Gdata.plateNumber = jo.getString("plate_number");
                                                    Gdata.photo = jo.getString("member_image");
                                                    Gdata.like = jo.getString("stars_vot");
                                                    Gdata.dislike = jo.getString("bad_vot");
                                                    Gdata.email = jo.getString("email");
                                                    Gdata.vcolor = jo.getString("color");
                                                    Gdata.address = jo.getString("place");
                                                    Gdata.status_type = "m";
                                                    Intent i = new Intent(SplashActivity.this, DriverMainActivity.class);
                                                    startActivity(i);
                                                    finish();

                                                }
                                            } catch (JSONException e) {
                                                Toast t = Toast.makeText(SplashActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                                                t.setGravity(Gravity.CENTER, 0, 0);
                                                t.show();
                                               e.printStackTrace();
                                            }
//                                        }else {
//                                            Toast t = Toast.makeText(SplashActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
//                                            t.setGravity(Gravity.CENTER, 0, 0);
//                                            t.show();
//                                        }
                                    }
                                });
                            }else {
                                Looper.prepare();
                                Toast t = Toast.makeText(SplashActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                                t.setGravity(Gravity.CENTER, 0, 0);
                                t.show();
                                Looper.loop();
                            }
                        }else {
                            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo activeNetworkInfo = connectivityManager
                                    .getActiveNetworkInfo();
                            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                                client.get(URLConstants.BASE_URL+"c_m_s/get_user_byid.php?u_id=" + id, new AsyncHttpResponseHandler() {
                                    @Override
                                    public void onFailure(Throwable throwable, String s) {
                                        Looper.prepare();
                                        Toast t = Toast.makeText(SplashActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                                        t.setGravity(Gravity.CENTER, 0, 0);
                                        t.show();
                                        Looper.loop();
                                    }

                                    @Override
                                    public void onSuccess(String s) {
                                        if (s.contains("{\"result\":")) {
                                            Logger.LOG_I("Response - c",s);
                                            JSONObject j = null;
                                            try {
                                                j = new JSONObject(s);
                                                if (j.getString("result").equals("true")) {
                                                    JSONArray jary = j.getJSONArray("data");
                                                    JSONObject jo = jary.getJSONObject(0);
                                                    Gdata.user_id = id;
                                                    Gdata.user_name = jo.getString("f_name");
                                                    Gdata.user_username = jo.getString("user_name");
                                                    Gdata.user_phone = jo.getString("mobile");
                                                    Gdata.user_photo = jo.getString("member_image");
                                                    Gdata.user_email = jo.getString("email");
                                                    Gdata.status_type = "c";
                                                    Gdata.count=0;
                                                    Gdata.user_lat=null;
                                                    Gdata.user_long=null;
                                                    Intent i = new Intent(SplashActivity.this, NeworderActivity.class);
                                                    startActivity(i);
                                                    finish();

                                                }
                                            } catch (JSONException e) {
                                                Looper.prepare();
                                                Toast t = Toast.makeText(SplashActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                                                t.setGravity(Gravity.CENTER, 0, 0);
                                                t.show();
                                                Looper.loop();
                                                e.printStackTrace();                                            }
                                        }else {
                                            Toast t = Toast.makeText(SplashActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                                            t.setGravity(Gravity.CENTER, 0, 0);
                                            t.show();
                                        }
                                    }
                                });
                            }else {
                                Looper.prepare();
                                Toast t = Toast.makeText(SplashActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                                t.setGravity(Gravity.CENTER, 0, 0);
                                t.show();
                                Looper.loop();
                            }
                        }

                    }

                }
            }
        };
        th.start();
//        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_left);
//        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_right);

    }
}
