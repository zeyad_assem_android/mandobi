package com.mandobiapp.driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class ClientorderActivity extends Activity {
    TextView title, name, address, phone, stype, from, to, trans, accept, reject, conname, conphone, price;
    ImageView image, transport;
    LinearLayout lin;
    ProgressBar progress;
    AsyncHttpClient client = new AsyncHttpClient();
    ProgressDialog progressDialog;
    SharedPreferences storedata;
    private static String filename = "mlogin";
    String id, type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientorder);
        storedata = getSharedPreferences(filename, 0);
        id = storedata.getString("id", "vgcvc");
        title = (TextView) findViewById(R.id.title);
        name = (TextView) findViewById(R.id.name);
        address = (TextView) findViewById(R.id.address);
        phone = (TextView) findViewById(R.id.phone);
        stype = (TextView) findViewById(R.id.shiptype);
        from = (TextView) findViewById(R.id.shipfrom);
        to = (TextView) findViewById(R.id.shipto);
        trans = (TextView) findViewById(R.id.trasport);
        accept = (TextView) findViewById(R.id.accept);
        reject = (TextView) findViewById(R.id.reject);
        price = (TextView) findViewById(R.id.price);
        conname = (TextView) findViewById(R.id.conname);
        conphone = (TextView) findViewById(R.id.conphone);
        image = (ImageView) findViewById(R.id.image);
        transport = (ImageView) findViewById(R.id.transimage);
        lin = (LinearLayout) findViewById(R.id.lin);
        progress = (ProgressBar) findViewById(R.id.progress);
        client.get("http://mandobiapp.com/c_m_s/get_user_byid.php?u_id=" + id, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                JSONObject j = null;
                try {
                    j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        Gdata.user_id = id;
                        Gdata.user_name = jo.getString("f_name");
                        Gdata.user_username = jo.getString("user_name");
                        Gdata.user_phone = jo.getString("mobile");
                        Gdata.user_photo = jo.getString("member_image");
                        Gdata.user_email = jo.getString("email");
                        Gdata.status_type = "c";

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        progressDialog = ProgressDialog.show(ClientorderActivity.this, getString(R.string.wait), getString(R.string.load), true);
        client.get(String.format("http://mandobiapp.com/c_m_s/view_msg_by_id.php?msg_id=%s&u_t=m&type=recieve", Gdata.message_id), new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                JSONObject j = null;
                try {
                    j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        name.setText(jo.getString("f_name"));
                        phone.setText(jo.getString("mobile"));
                        address.setText(jo.getString("place"));
                        title.setText(jo.getString("msg"));
                        conname.setText(jo.getString("recipient_user"));
                        conphone.setText(jo.getString("recipient_tel"));
                        price.setText(jo.getString("price_move"));
                        Picasso.with(ClientorderActivity.this).load("http://mandobiapp.com/all_images/" + jo.getString("member_image")).error(R.drawable.use).into(image, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                progress.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                progress.setVisibility(View.GONE);

                            }
                        });
                        Gdata.from_lat = jo.getString("from_latitude");
                        Gdata.from_lon = jo.getString("from_longitude");
                        Gdata.to_lat = jo.getString("to_latitude");
                        Gdata.to_lon = jo.getString("to_longitude");
                        Gdata.type = jo.getString("vehicle_type");
                        Geocoder geocoder;
                        List<Address> adresses;
                        geocoder = new Geocoder(ClientorderActivity.this,
                                Locale.ENGLISH);
                        adresses = geocoder.getFromLocation(Double.parseDouble(jo.getString("from_latitude")),
                                Double.parseDouble(jo.getString("from_longitude")), 1);
                        String m = (adresses.get(0).getAddressLine(0) + "  "
                                + adresses.get(0).getAddressLine(1) + "  "
                                + adresses.get(0).getAddressLine(2) + "  " + adresses
                                .get(0).getAddressLine(4));
                        from.setText(m);
                        Geocoder geocoder1;
                        List<Address> adresses1;
                        geocoder1 = new Geocoder(ClientorderActivity.this,
                                Locale.ENGLISH);
                        adresses1 = geocoder1.getFromLocation(Double.parseDouble(jo.getString("to_latitude")),
                                Double.parseDouble(jo.getString("to_longitude")), 1);
                        String m1 = (adresses1.get(0).getAddressLine(0) + "  "
                                + adresses1.get(0).getAddressLine(1) + "  "
                                + adresses1.get(0).getAddressLine(2) + "  " + adresses1
                                .get(0).getAddressLine(4));
                        to.setText(m1);
                        //from.setText(getAddress(ClientorderActivity.this, Double.parseDouble(jo.getString("from_latitude")), Double.parseDouble(jo.getString("from_longitude"))));
                        // to.setText(getAddress(ClientorderActivity.this, Double.parseDouble(jo.getString("to_latitude")), Double.parseDouble(jo.getString("to_longitude"))));
                        if (jo.getString("vehicle_type").equals("1")) {
                            trans.setText(getString(R.string.small));
                            transport.setBackgroundResource(R.drawable.carsmall);
                        } else if (jo.getString("vehicle_type").equals("2")) {
                            trans.setText(getString(R.string.medium));
                            transport.setBackgroundResource(R.drawable.carmedium);
                        } else {
                            trans.setText(getString(R.string.large));
                            transport.setBackgroundResource(R.drawable.carlarge);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
                lin.setVisibility(View.VISIBLE);
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = ProgressDialog.show(ClientorderActivity.this, getString(R.string.wait), getString(R.string.load), true);
                client.get(String.format("http://mandobiapp.com/c_m_s/re_reply_search.php?sender_id=%s&reciever_id=%s&msg_id=%s&lang=1", Gdata.user_id, Gdata.id, Gdata.message_id), new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String s) {
                        JSONObject j = null;
                        try {
                            j = new JSONObject(s);
                            if (j.getString("result").equals("true")) {
                                Gdata.message_id = j.getString("msg_id");
                                Intent i = new Intent(ClientorderActivity.this, mandobfollow.class);
                                startActivity(i);
                                finish();
                            } else {
                                Toast t = Toast.makeText(ClientorderActivity.this, j.getString("data"), Toast.LENGTH_LONG);
                                t.setGravity(Gravity.CENTER, 0, 0);
                                t.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                });

                // new GetOrders().execute();
//                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
//                        .permitAll().build();
//                StrictMode.setThreadPolicy(policy);
//                String response = null;
//                try {
//                    response = excutePost( "http://mandobiapp.com/c_m_s/re_reply_search1.php", "sender_id=" + URLEncoder.encode(Gdata.user_id, "UTF-8")
//                            + "&reciever_id=" + URLEncoder.encode(Gdata.id, "UTF-8"));
//                    if (response != null) {
//                        JSONObject j = new JSONObject(response);
//                        if (j.getString("result").equals("true")) {
//                            Intent i = new Intent(ClientorderActivity.this, ClientprofileActivity.class);
//                            startActivity(i);
//                            finish();
//                        } else {
//                        }
//                    }
//                }catch(UnsupportedEncodingException e){
//                    e.printStackTrace();
//                }catch(JSONException e){
//                    e.printStackTrace();
//                }

            }
        });
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ClientorderActivity.this, cancelling.class);
                startActivity(i);
                finish();
            }
        });
    }

    public String getAddress(Context ctx, double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        try {
            Geocoder geocoder = new Geocoder(ctx, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);

                String locality = address.getLocality();
                String city = address.getCountryName();
                String region_code = address.getCountryCode();
                String zipcode = address.getPostalCode();
                double lat = address.getLatitude();
                double lon = address.getLongitude();

                result.append(locality + " ");
                result.append(city + " " + region_code + " ");
                result.append(zipcode);

            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }

        return result.toString();
    }

    public String excutePost(String targetURL, String urlParameters) {
        URL url;
        HttpURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length", "" +
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();

        } catch (Exception e) {
            Looper.prepare();
            Toast t = Toast.makeText(ClientorderActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            e.printStackTrace();
            Looper.loop();
            e.printStackTrace();
            return null;

        } finally {

            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    class GetOrders extends AsyncTask<Void, String, String> {
        String s;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(ClientorderActivity.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            try {
                response = excutePost("http://mandobiapp.com/c_m_s/re_reply_search1.php?lang=1", "sender_id=" + URLEncoder.encode(Gdata.user_id, "UTF-8")
                        + "&reciever_id=" + URLEncoder.encode(Gdata.id, "UTF-8") + "&msg_id=" + URLEncoder.encode(Gdata.message_id, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        Intent i = new Intent(ClientorderActivity.this, ProfileActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Toast t = Toast.makeText(ClientorderActivity.this, j.getString("data"), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
            }
        }

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(ClientorderActivity.this, LastOrder.class);
        startActivity(i);
        finish();
    }
}



