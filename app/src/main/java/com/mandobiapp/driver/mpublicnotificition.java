package com.mandobiapp.driver;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class mpublicnotificition extends Activity {
    TextViewWithFont title, content, t;
    TextView menu;
    SharedPreferences storedata;
    private static String filename = "mlogin";
    String id, type;
    AsyncHttpClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicnotificition);
        storedata = getSharedPreferences(filename, 0);
        id = storedata.getString("id", "vgcvc");
        client = new AsyncHttpClient();
        content = (TextViewWithFont) findViewById(R.id.content);
        t = (TextViewWithFont) findViewById(R.id.t);
        t.setText(Gdata.t);
        content.setText(Gdata.body);
        storedata = getSharedPreferences(filename, 0);
        Gdata.id = storedata.getString("id", "vgcvc");
        client.get("http://mandobiapp.com/w_m_s/get_user_byid.php?u_id=" + id, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                JSONObject j = null;
                try {
                    j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        Gdata.id = id;
                        Gdata.name = jo.getString("f_name");
                        Gdata.username = jo.getString("user_name");
                        Gdata.isbusy = jo.getString("is_busy");
                        Gdata.phoneNumber = jo.getString("mobile");
                        Gdata.taxitype = jo.getString("taxi_type");
                        Gdata.plateNumber = jo.getString("plate_number");
                        Gdata.photo = jo.getString("member_image");
                        Gdata.like = jo.getString("stars_vot");
                        Gdata.dislike = jo.getString("bad_vot");
                        Gdata.email = jo.getString("email");
                        Gdata.vcolor = jo.getString("color");
                        Gdata.address = jo.getString("place");
                        Gdata.status_type = "m";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
