package com.mandobiapp.driver;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;


public class ChangeActivity extends Activity {
    LinearLayout lin, lin1;
    TextView check, check1;
Context con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change);
        con = this ;
        lin = (LinearLayout) findViewById(R.id.lin);
        lin1 = (LinearLayout) findViewById(R.id.lin1);
        check = (TextView) findViewById(R.id.check);
        check1 = (TextView) findViewById(R.id.check1);
       if (Locale.getDefault().getLanguage().equals("ar")){
           check1.setBackgroundResource(R.drawable.check);
       }else {
           check.setBackgroundResource(R.drawable.check);
       }
        lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String languageToLoad = "en"; // your language
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources()
                        .updateConfiguration(
                                config,
                                getBaseContext().getResources()
                                        .getDisplayMetrics());
               Gdata.back(ChangeActivity.this);
            }
        });
        lin1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String languageToLoad = "ar"; // your language
                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources()
                        .updateConfiguration(
                                config,
                                getBaseContext().getResources()
                                        .getDisplayMetrics());
                Gdata.back(ChangeActivity.this);

            }
        });

    }

    @Override
    public void onBackPressed() {
        Gdata.back(ChangeActivity.this);
    }
}
