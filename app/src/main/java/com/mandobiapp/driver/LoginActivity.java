package com.mandobiapp.driver;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

import com.mandobiapp.driver.engine.constants.URLConstants;
import com.mandobiapp.driver.ui.activities.driver.main.DriverMainActivity;
import com.mandobiapp.driver.ui.activities.uiUitilities.RobotoTextView;


public class LoginActivity extends Activity {
    EditText username, password;
    RobotoTextView forget;
    Button login;
    ProgressBar progress;
    SharedPreferences storedata;
    private static String filename = "mlogin";
    ProgressDialog progressDialog;
    String s = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        username = (EditText) findViewById(R.id.username);
        username.setSingleLine(true);
        password = (EditText) findViewById(R.id.password);
//        reg = (TextViewWithFont) findViewById(R.id.reg);
        login = (Button) findViewById(R.id.login);
        progress = (ProgressBar) findViewById(R.id.progress);
        progress.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.mustard),
                android.graphics.PorterDuff.Mode.SRC_IN);
        forget = (RobotoTextView) findViewById(R.id.forget);
        progress.setVisibility(View.INVISIBLE);
        //slidepub.slidemenu(LoginActivity.this);
        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(LoginActivity.this);

                //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle(getString(R.string.reset));

                // Setting Dialog Message
//                alertDialog.setMessage("Enter Password");
                final EditText input = new EditText(LoginActivity.this);
                input.setHint(getString(R.string.em));
                alertDialog.setView(input);
                if (Locale.getDefault().getLanguage().equals("ar")) {
                    input.setPadding(0, 0, 10, 0);
                    input.setGravity(Gravity.RIGHT | Gravity.CENTER);
                } else {
                    input.setPadding(10, 0, 0, 0);
                    input.setGravity(Gravity.LEFT | Gravity.CENTER);
                }
                alertDialog.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (input.getText().toString().equals("") && input.getText().toString().replaceAll(" ", "").equals("")) {
                                    Toast t = Toast.makeText(LoginActivity.this, getString(R.string.complete), Toast.LENGTH_LONG);
                                    t.setGravity(Gravity.CENTER, 0, 0);
                                    t.show();
                                } else {
                                    s = input.getText().toString();
                                    new GetOrder().execute();
                                }
                            }
                        });
                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Write your code here to execute after dialog
                                dialog.cancel();
                            }
                        });
                alertDialog.show();
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager
                        .getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    if (username.getText().toString().equals("") && username.getText().toString().replaceAll(" ", "").equals("") && password.getText().toString().equals("") && password.getText().toString().replaceAll(" ", "").equals("")) {
                        Toast t = Toast.makeText(LoginActivity.this, getString(R.string.complete), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else {
                        new GetOrders().execute();
                    }
                } else {
                    Toast t = Toast.makeText(LoginActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }
            }
        });
        Gdata.context = "ml";

    }

    public String h(String url, String username, String password) {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("username", new StringBody(username));
            multipartEntity.addPart("password", new StringBody(password));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = null;
            response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            Looper.prepare();
            progressDialog.dismiss();
            Toast t = Toast.makeText(LoginActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            Looper.loop();
        }

        return json;
    }


    class GetOrders extends AsyncTask<Void, String, String> {
        String s;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(LoginActivity.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = h(URLConstants.BASE_URL+"w_m_s/login_ws.php", username.getText().toString(), password.getText().toString());
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        storedata = getSharedPreferences(filename, 0);
                        SharedPreferences.Editor edit = storedata.edit();
                        edit.putString("id", jo.getString("member_id"));
                        edit.putString("type", "m");
                        edit.commit();
                        Gdata.id = jo.getString("member_id");
                        Gdata.name = jo.getString("f_name");
                        Gdata.username = jo.getString("user_name");
                        Gdata.isbusy = jo.getString("is_busy");
                        Gdata.phoneNumber = jo.getString("mobile");
                        Gdata.taxitype = jo.getString("taxi_type");
                        Gdata.plateNumber = jo.getString("plate_number");
                        Gdata.photo = jo.getString("member_image");
                        Gdata.like = jo.getString("stars_vot");
                        Gdata.dislike = jo.getString("bad_vot");
                        Gdata.email = jo.getString("email");
                        Gdata.vcolor = jo.getString("color");
                        Gdata.address = jo.getString("place");
                        Gdata.status_type = "m";
//                        Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                        Intent i = new Intent(LoginActivity.this, DriverMainActivity.class);
                        Gdata.backlist.removeAll(Gdata.backlist);
                        startActivity(i);
                        finish();
                    } else {
                        Toast t = Toast.makeText(LoginActivity.this, getString(R.string.sure), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }

    }

    class GetOrder extends AsyncTask<Void, String, String> {
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(LoginActivity.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected String doInBackground(Void... voids) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            String response = null;
            response = hh(URLConstants.BASE_URL+"w_m_s/reset_password.php?lang=0", s);
            return response;

        }

        protected void onPostExecute(String orders) {
            if (orders.contains("{\"result\":")) {
                JSONObject j = null;
                try {
                    j = new JSONObject(orders);
                    if (j.getString("result").equals("true")) {
                        Toast t = Toast.makeText(LoginActivity.this, getString(R.string.c), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    } else {
                        Toast t = Toast.makeText(LoginActivity.this, j.getString("data"), Toast.LENGTH_LONG);
                        t.setGravity(Gravity.CENTER, 0, 0);
                        t.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }
        }

    }

    @Override
    public void onBackPressed() {
        Gdata.back(LoginActivity.this);
    }

    public String hh(String url, String email) {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("email", new StringBody(email));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            Looper.prepare();
            Toast t = Toast.makeText(LoginActivity.this, getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            e.printStackTrace();
            Looper.loop();
        }

        return json;
    }
}
