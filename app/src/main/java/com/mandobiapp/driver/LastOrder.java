package com.mandobiapp.driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class LastOrder extends Activity {
    orderAdapt orderAdapter;
    ListView list;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        slideclient.slidemenu(LastOrder.this);
        Gdata.context = "clo";
        list = (ListView) findViewById(R.id.list);
        orderAdapter = new orderAdapt(LastOrder.this, R.id.list);
        list.setAdapter(orderAdapter);
       new GetOrders().execute();
    }

    @Override
    public void onBackPressed() {
        Gdata.back(LastOrder.this);
    }

    class GetOrders extends AsyncTask<Void, String, JSONArray> {
        String s;
        order news;
        ArrayList<Integer> rr = new ArrayList<Integer>();

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(LastOrder.this, getString(R.string.wait), getString(R.string.load), true);
        }

        protected JSONArray doInBackground(Void... voids) {

            JSONArray orders = null;
            JSONParser json = new JSONParser();
            orders = json.makeHttpRequest(
                   String.format("http://mandobiapp.com/c_m_s/view_inbox.php?member_id=%s&type=recieve&u_t=w&t=1",Gdata.user_id), "GET", "");
            return orders;
        }

        protected void onPostExecute(JSONArray orders) {

            if (orders == null) {
                return;
            }
            orderAdapter.clear();


            JSONObject order = null;
            for (int i = 0; i < orders.length(); i++) {
                try {
                    order = orders.getJSONObject(i);
                        news = new order();
                        news.date = order.getString("date");
                        news.title = order.getString("msg");
                        news.id = order.getString("mail_id");
                        news.seen = order.getString("is_view");
                        news.sender_id = order.getString("sender_id");
                        orderAdapter.add(news);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            progressDialog.dismiss();
        }
    }

}
