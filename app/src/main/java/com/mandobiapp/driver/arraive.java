package com.mandobiapp.driver;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class arraive extends Activity {
    TextViewWithFont title, name, done;
    TextView call, menu;
    ImageView image;
    GoogleMap googleMap;
    Marker marker;
    AsyncHttpClient client = new AsyncHttpClient();
    SharedPreferences storedata;
    private static String filename = "mlogin";
    String id, type;
    ProgressBar progress;
    LinearLayout lin;
    ProgressDialog progressDialog;
    int x;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arraive);
        storedata = getSharedPreferences(filename, 0);
        id = storedata.getString("id", "vgcvc");
        type = storedata.getString("type", "vgcvc");
        name = (TextViewWithFont) findViewById(R.id.name);
        done = (TextViewWithFont) findViewById(R.id.done);
        image = (ImageView) findViewById(R.id.photo);
        progress = (ProgressBar) findViewById(R.id.progress);
        lin = (LinearLayout) findViewById(R.id.lin);
        slidemandob.slidemenu(arraive.this);
        progressDialog = ProgressDialog.show(arraive.this, getString(R.string.wait), getString(R.string.load), true);
        client.get("http://mandobiapp.com/w_m_s/get_user_byid.php?u_id=" + id, new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(Throwable throwable, String s) {
                Looper.prepare();
                Toast t = Toast.makeText(arraive.this, getString(R.string.check), Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                Looper.loop();
            }

            @Override
            public void onSuccess(String s) {
                JSONObject j = null;
                try {
                    j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        Gdata.id = id;
                        Gdata.name = jo.getString("f_name");
                        Gdata.username = jo.getString("user_name");
                        Gdata.isbusy = jo.getString("is_busy");
                        Gdata.phoneNumber = jo.getString("mobile");
                        Gdata.taxitype = jo.getString("taxi_type");
                        Gdata.plateNumber = jo.getString("plate_number");
                        Gdata.photo = jo.getString("member_image");
                        Gdata.like = jo.getString("stars_vot");
                        Gdata.dislike = jo.getString("bad_vot");
                        Gdata.email = jo.getString("email");
                        Gdata.vcolor = jo.getString("color");
                        Gdata.address = jo.getString("place");
                        Gdata.lat = jo.getString("latitude");
                        Gdata.longt = jo.getString("longitude");
                        Gdata.status_type = "m";

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        client.get(String.format("http://mandobiapp.com/c_m_s/view_msg_by_id.php?msg_id=%s&u_t=w&type=recieve", Gdata.message_id), new AsyncHttpResponseHandler() {
            @Override
            public void onFailure(Throwable throwable, String s) {
                Looper.prepare();
                Toast t = Toast.makeText(arraive.this, getString(R.string.check), Toast.LENGTH_LONG);
                t.setGravity(Gravity.CENTER, 0, 0);
                t.show();
                Looper.loop();
            }

            @Override
            public void onSuccess(String s) {
                JSONObject j = null;
                try {
                    j = new JSONObject(s);
                    if (j.getString("result").equals("true")) {
                        JSONArray jary = j.getJSONArray("data");
                        JSONObject jo = jary.getJSONObject(0);
                        name.setText(jo.getString("f_name"));
                        Gdata.from_lat = jo.getString("from_latitude");
                        Gdata.from_lon = jo.getString("from_longitude");
                        Gdata.to_lat = jo.getString("to_latitude");
                        Gdata.to_lon = jo.getString("to_longitude");
                        Gdata.type = jo.getString("vehicle_type");
                        Picasso.with(arraive.this).load("http://mandobiapp.com/all_images/" + jo.getString("member_image")).error(R.drawable.use).into(image, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                progress.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                progress.setVisibility(View.GONE);

                            }
                        });
                        if (jo.getString("vehicle_type").equals("1")) {
                            x = R.drawable.smal_car;
                        } else if (jo.getString("vehicle_type").equals("2")) {
                            x = R.drawable.med_car;
                        } else {
                            x = R.drawable.large_car;
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();
                lin.setVisibility(View.VISIBLE);
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String format = "geo:0,0?q=" + Gdata.to_lat + "," + Gdata.to_lon + "( arraive me)";
                Uri uri = Uri.parse(format);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (googleMap == null) {
                        //                Hashed by zeyad
//                        googleMap = ((MapFragment) getFragmentManager().findFragmentById(
//                                R.id.maps)).getMap();
                    }
                    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                    googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon))).title(
                            "From Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon))).title(
                            "To Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.marker)));
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(Gdata.lat), Double.parseDouble(Gdata.longt))).title(
                            "MY LOCATION").icon(BitmapDescriptorFactory.fromResource(x)));
                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    googleMap.getUiSettings().setZoomGesturesEnabled(true);
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(Double.parseDouble(Gdata.lat), Double.parseDouble(Gdata.longt))).zoom(12).build();
                    googleMap.animateCamera(CameraUpdateFactory
                            .newCameraPosition(cameraPosition));
                    LatLng origin = new LatLng(Double.parseDouble(Gdata.from_lat), Double.parseDouble(Gdata.from_lon));
                    LatLng dest = new LatLng(Double.parseDouble(Gdata.to_lat), Double.parseDouble(Gdata.to_lon));
                    String url = getDirectionsUrl(origin, dest);
                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 3000);
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + ","
                + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + parameters;
        return url;
    }

    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                parser parser = new parser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            Log.e("results", result + "");
            if (result.size() < 1) {
                Toast.makeText(arraive.this, "No Points", Toast.LENGTH_SHORT)
                        .show();
                return;
            }
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);
                Log.e("points", path + "");
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    if (j == 0) {

                    } else if (j == 1) {
                    } else if (j > 1) {
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                }
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.parseColor("#f9b21a"));
            }
            googleMap.addPolyline(lineOptions);
        }
    }
}
