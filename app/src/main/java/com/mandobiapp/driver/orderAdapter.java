package com.mandobiapp.driver;

import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class orderAdapter extends ArrayAdapter<order> {
    private final LayoutInflater mInflater;
    Context context;
    // AsyncHttpClient client;

    public orderAdapter(Context context, int resource) {
        super(context, resource);
        // TODO Auto-generated constructor stub
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        // client = new AsyncHttpClient();
    }

    private static class ViewHolder {
        @SuppressWarnings("unused")
        public TextView title;
        @SuppressWarnings("unused")
        public TextView date;
        public TextView seen;
        public LinearLayout lin;

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @SuppressLint({"ResourceAsColor", "NewApi"})
    @Override
    public View getView(final int position, View v, final ViewGroup parent) {
        ViewHolder holder = null;
        if (v == null)
            v = mInflater.inflate(R.layout.order, null);
        else
            holder = (ViewHolder) v.getTag();

        final order news = this.getItem(position);
        if (news != null) {
            TextView title = (TextView) v.findViewById(R.id.title);
            TextView date = (TextView) v.findViewById(R.id.date);
            TextView seen = (TextView) v.findViewById(R.id.seen);
            LinearLayout lin = (LinearLayout)v.findViewById(R.id.lin);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Gdata.message_id = news.id;
                    Gdata.user_id = news.sender_id;
                    Intent i = new Intent(context, DetailsorderActivity.class);
                    context.startActivity(i);
                    ((Activity)context).finish();
                }
            });
            if (news.seen.equals("1")){
                seen.setBackgroundResource(R.drawable.eye);
            }else{
                seen.setBackgroundResource(R.drawable.eye_not);
            }
            if (position % 2 == 1) {
                v.setBackgroundColor(Color.parseColor("#f4f4f4"));
            } else {
                v.setBackgroundColor(Color.parseColor("#fafafa"));
            }
            title.setText(news.title);
            date.setText(news.date);
            holder = new ViewHolder();
            holder.title = title;
            holder.date = date;
            holder.seen = seen;
            holder.lin = lin;
            v.setTag(holder);
        }

        return v;
    }

    public void setData(List<order> data) {
        clear();

        if (data != null) {
            for (int i = 0; i < data.size(); i++) {
                add(data.get(i));
            }
        }
    }
}
