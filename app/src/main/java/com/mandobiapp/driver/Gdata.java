package com.mandobiapp.driver;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mandobiapp.driver.ui.activities.MainActivity;

/**
 * Created by elkholy on 20/12/2015.
 */
public class Gdata {
    public static String name;
    public static String address;
    public static String vcolor;
    public static String plateNumber;
    public static String username;
    public static String phoneNumber;
    public static String taxitype;
    public static String isbusy;
    public static String my_wallet;
    public static String id;
    public static String like;
    public static String dislike;
    public static String photo;
    public static String message;
    public static String longt;
    public static String lat;
    public static String email;
    public static String user_id;
    public static String title;
    public static String user_email;
    public static String user_username;
    public static String user_name;
    public static String user_conname;
    public static String user_conphone;
    public static String user_phone;
    public static String user_photo;
    public static String user_lat;
    public static String user_long;
    public static String suser_lat;
    public static String suser_long;
    public static String message_id;
    public static String t;
    public static String body;
    public static int count = 0;
    public static String context;
    public static String lang;
    public static String from_lat;
    public static String from_lon;
    public static String to_lat;
    public static String to_lon;
    public static String type;
    public static String content;
    public static String order_id;
    public static String status_type;
    public static int price;
    public final static ArrayList<String> backlist = new ArrayList<>();

    public static boolean emailValidator(String mail) {
        String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(mail);
        return matcher.matches();
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    public static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static String h(final Context context, String url, String access, String id) {
        InputStream is = null;
        String json = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);
            MultipartEntity multipartEntity = new MultipartEntity();
            multipartEntity.addPart("member_id", new StringBody(id));
            multipartEntity.addPart("access_key", new StringBody(access));
            httpPostRequest.setEntity(multipartEntity);
            HttpResponse response = httpClient.execute(httpPostRequest);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            int s = response.getStatusLine().getStatusCode();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            Looper.prepare();
            Toast t = Toast.makeText(context, context.getString(R.string.check), Toast.LENGTH_LONG);
            t.setGravity(Gravity.CENTER, 0, 0);
            t.show();
            e.printStackTrace();
            Looper.loop();
        }

        return json;
    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void back(final Context context) {
        if (Gdata.backlist.size() != 0) {
            if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("m")) {
                Intent i = new Intent(context, MainActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("ml")) {
                Intent i = new Intent(context, LoginActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("mr")) {
                Intent i = new Intent(context, RegisterActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("cl")) {
                Intent i = new Intent(context, ClientloginActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("cr")) {
                Intent i = new Intent(context, ClientregActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("mp")) {
                Intent i = new Intent(context, ProfileActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("mo")) {
                Intent i = new Intent(context, OrderActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("mh")) {
                Intent i = new Intent(context, HomeActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("me")) {
                Intent i = new Intent(context, Editprofile.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("md")) {
                Intent i = new Intent(context, DetailsorderActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("co")) {
                Intent i = new Intent(context, ClientmyorderActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("cp")) {
                Intent i = new Intent(context, ClientprofileActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("cn")) {
                Intent i = new Intent(context, NeworderActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("cf")) {
                Intent i = new Intent(context, FollowerActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("ma")) {
                Intent i = new Intent(context, About.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("mho")) {
                Intent i = new Intent(context, howtoregister.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("mpo")) {
                Intent i = new Intent(context, policy.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("mmc")) {
                Intent i = new Intent(context, ProfitsActivity.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            } else if (Gdata.backlist.get(Gdata.backlist.size() - 1).equals("clo")) {
                Intent i = new Intent(context, LastOrder.class);
                context.startActivity(i);
                ((Activity) context).finish();
                Gdata.backlist.remove(Gdata.backlist.size() - 1);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            context.startActivity(intent);
            ((Activity) context).finish();
            System.runFinalizersOnExit(true);
            System.exit(0);
            android.os.Process.killProcess(android.os.Process.myPid());
            Gdata.count = 0;
            Gdata.user_lat = null;
            Gdata.user_long = null;
            Gdata.lat = null;
            Gdata.longt = null;
        }

    }
}
